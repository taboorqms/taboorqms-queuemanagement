package com.taboor.qms.queue.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.taboor.qms.core.auth.JwtAuthenticationFilter;
import com.taboor.qms.core.utils.PrivilegeName;

@EnableWebSecurity
class AuthWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService myUserDetailsService;

	@Autowired
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
	 @Bean
	 public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
	        ThreadPoolTaskScheduler threadPoolTaskScheduler
	          = new ThreadPoolTaskScheduler();
	        threadPoolTaskScheduler.setPoolSize(5);
	        threadPoolTaskScheduler.setThreadNamePrefix(
	          "ThreadPoolTaskScheduler");
	        return threadPoolTaskScheduler;
	    }
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()
				.antMatchers("/h2-console/**", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**")
				.permitAll().antMatchers("/ticket/customer/book").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/guest/ticket/kiosik").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/guest/counter/config").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/guest/get/counter/ticket").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/ticket/transfer").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/get/config").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/queue/customer/pending/get")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/ticket/feedback/add")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/ticket/feedback/add/counter").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/queue/tickets/get")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name(),
						PrivilegeName.Queue_View.name(), PrivilegeName.Agent_Serve.name())
				.antMatchers("/ticketFastpass/fastpass/apply").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/cancel/ticket")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/ticket/customer/stepOut").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/stepIn").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/get/timer").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/serve/ticket")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/ticket/customer/ticket/timeline")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/guest/ticket/get").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticket/customer/get/ticket")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/branch/get/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/branch/get/near").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/branch/getServices/id")
				.hasAnyRole(PrivilegeName.Customer_Actions.name(), PrivilegeName.Guest_Ticket.name())
				.antMatchers("/branch/getCompleteDetails/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/requiredDocument/get/BranchAndService").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/servicecenter/get/all").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/service/get/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/service/get/all").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/taboorConfiguration/get/add").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/taboorConfiguration/get/configKey").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/taboorConfiguration/get/all").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticketFastpass/get/id").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/ticketFastpass/get/user").hasAnyRole(PrivilegeName.Customer_Actions.name())
				.antMatchers("/guest/get/branches").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/ticketFastpass/fastpass/cancel").hasAnyRole(PrivilegeName.Agent_Serve.name())
				// to be removed
				.antMatchers("/guest/send/get/feedback").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/guest/send/change/ticket").hasAnyRole(PrivilegeName.Branch_Managment.name())
				// ByPass Routes
				.antMatchers("/branch/get/id", "/branch/getCompleteDetails/id", "/branch/getServices/id",
						"/branch/get/near", "/service/get/id", "/service/getName/all", "/ticketFastpass/get/id",
						"/requiredDocument/get/BranchAndService", "servicecenter/get/all", "/guest/ticket/add",
						"/taboorConfiguration/**")
				.permitAll().anyRequest().denyAll();
		httpSecurity.headers().frameOptions().disable();
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
	}

}