package com.taboor.qms.queue.management.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.TicketStepOut;
import com.taboor.qms.core.payload.AddTicketFeedbackCounterPayload;
import com.taboor.qms.core.payload.AddTicketFeedbackPayload;
import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.payload.ServeTicketPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;
import com.taboor.qms.queue.management.response.GetTicketListResponse;

@Service
public interface TicketService {

	GenStatusResponse bookCustomerTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
			throws TaboorQMSServiceException, Exception;

	Ticket bookGuestCustomerTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
			throws TaboorQMSServiceException, Exception;

	Ticket getTicketById(Long ticketId) throws TaboorQMSServiceException, Exception;

	TicketStepOut getStepOutTicketById(Long ticketId) throws TaboorQMSServiceException, Exception;

	GetTicketListResponse getTicketByIdRest(Long ticketId) throws TaboorQMSServiceException, Exception;

	GetTicketEntityListResponse transferTicket(Long ticketId, Long newBranchId) throws TaboorQMSServiceException, Exception;

	int deleteById(Long ticketId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne getTicketFastpassEstimation(long queueId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne stepOutCustomerTicket(@Valid StepInOutTicketPayload stepOutTicketPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse stepInCustomerTicket(@Valid StepInOutTicketPayload stepInTicketPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getCustomerTicketTimer(@Valid StepInOutTicketPayload stepInTicketPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse cancelCustomerTicket(@Valid StepInOutTicketPayload stepInTicketPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse addTicketFeedback(@Valid AddTicketFeedbackPayload addTicketFeedbackPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse addTicketFeedbackCounter(@Valid AddTicketFeedbackCounterPayload addTicketFeedbackPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne serveCustomerTicket(@Valid ServeTicketPayload serveTicketPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne servedCustomerTicket(@Valid ServeTicketPayload serveTicketPayload)
			throws TaboorQMSServiceException, Exception;

	Ticket saveTicketH2(Ticket ticket) throws TaboorQMSServiceException, Exception;

	TicketStepOut saveTicketStepOut(TicketStepOut ticketStepOut) throws TaboorQMSServiceException, Exception;
	
	com.taboor.qms.core.model.Ticket getTicketEntityById(Long ticketId) throws TaboorQMSServiceException, Exception;
	
	com.taboor.qms.core.model.Ticket saveTicket(Ticket ticket) throws TaboorQMSServiceException, Exception;

	ServiceCenterConfiguration getConfigEntityById(Long ticketId) throws TaboorQMSServiceException, Exception;
        
        void expireTickets() throws TaboorQMSServiceException, Exception;
        
        void deleteTickets() throws TaboorQMSServiceException, Exception;

}
