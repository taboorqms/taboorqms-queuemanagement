package com.taboor.qms.queue.management.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.core.response.GetGuestUserTicketResponse;
import com.taboor.qms.core.response.LoginGuestUserResponse;
import com.taboor.qms.queue.management.payload.AddCounterConfigPayload;
import com.taboor.qms.queue.management.payload.AddGuestTicketPayload;
import com.taboor.qms.queue.management.payload.GetCounterTicketPayload;
import com.taboor.qms.queue.management.response.GetTicketCounterResponse;
import com.taboor.qms.queue.management.service.GuestTicketService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/guest")
@CrossOrigin(origins = "*")
public class GuestTicketController {

	private static final Logger logger = LoggerFactory.getLogger(GuestTicketController.class);

	@Autowired
	GuestTicketService guestTicketService;

	@ApiOperation(value = "Add Guest Ticket", response = LoginGuestUserResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Add Guest fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/ticket/add", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody LoginGuestUserResponse addGuestTicket(@RequestBody AddGuestTicketPayload addGuestTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Guest Ticket - API");
		return guestTicketService.addGuestTicket(addGuestTicketPayload);
	}
	
	@ApiOperation(value = "Get Kiosik Ticket", response = GetGuestUserTicketResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get Guest Tickets fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/ticket/kiosik", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetGuestUserTicketResponse getKiosikGuestTicket(
			@RequestBody @Valid BookCustomerTicketPayload bookCustomerTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Guest Kiosik Ticket - API");
		return guestTicketService.getKiosikTicket(bookCustomerTicketPayload);
	}
	
	// TO BE MOVED to KCAT MS
	
	@ApiOperation(value = "Get All Branches Counters of BranchAdmin.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All Branches Counters fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/branches", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<GetBranchAdminCounterListResponse> getAllServices() throws Exception, Throwable {
		logger.info("Calling Get All Branches of Branch Admin - API");
		return guestTicketService.getBranchByEmp();
	}
	
	@ApiOperation(value = "Add Branch Counter Config", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Add Branch Counter Config Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/counter/config", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse addCounterConfig(@RequestBody AddCounterConfigPayload addCounterConfigPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Branch Counter Config - API");
		return guestTicketService.addCounterConfig(addCounterConfigPayload);
	}

	@ApiOperation(value = "Get Ticket Counter Serving", response = GetTicketCounterResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get Counter Ticket Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/counter/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketCounterResponse getCounterTicket(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Ticket Counter Serving - API");
		return guestTicketService.getCounterTicket(payload);
	}
	
	//Test Notifications (to be removed)
	@RequestMapping(value = "/send/change/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendTicketChange(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling sending ticket serving & counter no - API");
		return guestTicketService.sendTicketChange(payload);
	}
	
	@RequestMapping(value = "/send/get/feedback", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendFeedbackGet(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling sending feedback after serving - API");
		return guestTicketService.sendFeedbackGet(payload);
	}
	
	

}
