package com.taboor.qms.queue.management.serviceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taboor.qms.queue.management.service.TaboorConfigurationService;

@Service
public class TaboorConfigurationServiceImpl implements TaboorConfigurationService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

//	@Override
//	public GenStatusResponse addTaboorConfiguration(@Valid AddTaboorConfigurationPayload addTaboorConfigurationPayload)
//			throws TaboorQMSServiceException, Exception {
//		TaboorConfiguration taboorConfiguration = new TaboorConfiguration();
//		taboorConfiguration.setConfigKey(addTaboorConfigurationPayload.getConfigKey());
//		taboorConfiguration.setConfigValue(addTaboorConfigurationPayload.getConfigValue());
//		taboorConfiguration.setConfigDescription(addTaboorConfigurationPayload.getConfigDescription());
//
//		HttpEntity<TaboorConfiguration> entity = new HttpEntity<>(taboorConfiguration,
//				HttpHeadersUtils.getApplicationJsonHeader());
//
//		RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/taboorconfigurations/save", entity,
//				TaboorConfiguration.class, xyzErrorMessage.TABOORCONFIGURATION_NOT_CREATED.getErrorCode(),
//				xyzErrorMessage.TABOORCONFIGURATION_NOT_CREATED.getErrorDetails());
//
//		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
//				xyzResponseMessage.TABOOR_CONFIGURATION_ADDED.getMessage());
//	}
//
//	@Override
//	public GetTaboorConfigurationListResponse getByConfigKey(@Valid String configKey)
//			throws TaboorQMSServiceException, Exception {
//
//		TaboorConfiguration getTaboorConfigurationResponse = RestUtil.get(
//				dbConnectorMicroserviceURL + "/tab/taboorconfigurations/get/configKey?configKey=" + configKey,
//				TaboorConfiguration.class);
//
//		List<TaboorConfiguration> taboorConfigurationList = new ArrayList<TaboorConfiguration>();
//		taboorConfigurationList.add(getTaboorConfigurationResponse);
//		return new GetTaboorConfigurationListResponse(xyzResponseCode.SUCCESS.getCode(),
//				xyzResponseMessage.TABOOR_CONFIGURATION_FETCHED.getMessage(), taboorConfigurationList);
//	}
//
//	@Override
//	public GetTaboorConfigurationListResponse getAll() throws TaboorQMSServiceException, Exception {
//
//		List getTaboorConfigurationsResponse = RestUtil
//				.get(dbConnectorMicroserviceURL + "/tab/taboorconfigurations/get/all", List.class);
//
//		List<TaboorConfiguration> taboorConfigurationList = GenericMapper.convertListToObject(
//				getTaboorConfigurationsResponse, new TypeReference<List<TaboorConfiguration>>() {
//				});
//
//		return new GetTaboorConfigurationListResponse(xyzResponseCode.SUCCESS.getCode(),
//				xyzResponseMessage.TABOOR_CONFIGURATION_FETCHED.getMessage(), taboorConfigurationList);
//	}

}
