package com.taboor.qms.queue.management.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.response.GetServiceCentreListResponse;

@Service
public interface ServiceCenterService {
	
	public GetServiceCentreListResponse getAllServicecenters() throws TaboorQMSServiceException, Exception;
}
