package com.taboor.qms.queue.management.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GetAllServicesResponse;
import com.taboor.qms.core.response.GetServiceListResponse;

@Service
public interface ServiceTABService {

	public GetServiceListResponse getServiceTABDetails(@Valid Long serviceId)
			throws TaboorQMSServiceException, Exception;

	public ServiceTAB getServiceTAB(@Valid Long serviceId) throws TaboorQMSServiceException, Exception;

	public GetAllServicesResponse getAllServiceTAB() throws TaboorQMSServiceException, Exception;
}
