package com.taboor.qms.queue.management.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.queue.management.model.QueueTicketMapper;

@Service
public interface QueueTicketMapperService {

	public QueueTicketMapper getByTicketId(Long ticketId) throws TaboorQMSServiceException, Exception;

	public int deleteById(Long queueTicketMapperId) throws TaboorQMSServiceException, Exception;

	public int deleteTicketByTicketId(Long ticketId) throws TaboorQMSServiceException, Exception;

	public int counterQueueFastpassTickets(Long queueId) throws TaboorQMSServiceException, Exception;

	com.taboor.qms.core.model.QueueTicketMapper saveQueueTicketMapper(
			com.taboor.qms.core.model.QueueTicketMapper queueTicketMapper) throws TaboorQMSServiceException, Exception;
	
	QueueTicketMapper saveQueueTicketMapperH2(QueueTicketMapper queueTicketMapper)
			throws TaboorQMSServiceException, Exception;
}
