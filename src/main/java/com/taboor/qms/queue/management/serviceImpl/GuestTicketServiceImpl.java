package com.taboor.qms.queue.management.serviceImpl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.JwtTokenProvider;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterConfig;
import com.taboor.qms.core.model.GuestTicket;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.core.response.GetGuestUserTicketResponse;
import com.taboor.qms.core.response.LoginGuestUserResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.PushNotification;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.payload.AddCounterConfigPayload;
import com.taboor.qms.queue.management.payload.AddGuestTicketPayload;
import com.taboor.qms.queue.management.payload.GetCounterTicketPayload;
import com.taboor.qms.queue.management.repository.TicketRepository;
import com.taboor.qms.queue.management.response.GetTicketCounterResponse;
import com.taboor.qms.queue.management.service.GuestTicketService;
import com.taboor.qms.queue.management.service.TicketService;

import net.bytebuddy.utility.RandomString;

@Service
public class GuestTicketServiceImpl implements GuestTicketService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	TicketService ticketService;

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	private JwtTokenProvider tokenProvider;

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public LoginGuestUserResponse addGuestTicket(AddGuestTicketPayload addGuestTicketPayload)
			throws TaboorQMSServiceException, Exception {

		if (addGuestTicketPayload.getTicketNumber() == null) {
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());
		}

		Optional<Ticket> ticket = ticketRepository.findByTicketNumber(addGuestTicketPayload.getTicketNumber());

		if (!ticket.isPresent())
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());

		if (StringUtils.hasText(addGuestTicketPayload.getSessionToken())) {

			Long sessionId = tokenProvider.getSessionIdFromJWT(addGuestTicketPayload.getSessionToken());
			Session session = RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/id?sessionId=" + sessionId,
					Session.class, xyzErrorMessage.SESSION_TOKEN_INVALID.getErrorCode(),
					xyzErrorMessage.SESSION_TOKEN_INVALID.getErrorDetails());

			List<?> getUserPendingTicketResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
					+ "/tab/queueticketmapper/getTickets/pending/user?userId=" + session.getUser().getUserId(),
					List.class);

			if (getUserPendingTicketResponse != null) {
				List<Ticket> ticketList = GenericMapper.convertListToObject(getUserPendingTicketResponse,
						new TypeReference<List<Ticket>>() {
						});
				if (ticketList.size() > 0)
					throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_ALREADY_QUEUE.getErrorCode() + "::"
							+ xyzErrorMessage.TICKET_ALREADY_QUEUE.getErrorDetails());
			}

			com.taboor.qms.core.model.Ticket getTicketResponse = RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/tickets/get/id?ticketId=" + ticket.get().getTicketId(),
					com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode(),
					xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());

			TicketUserMapper ticketUserMapper = new TicketUserMapper();
			ticketUserMapper.setTicket(getTicketResponse);
			ticketUserMapper.setUser(session.getUser());

			HttpEntity<TicketUserMapper> ticketUserMapperEntity = new HttpEntity<>(ticketUserMapper,
					HttpHeadersUtils.getApplicationJsonHeader());

			RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/ticketusermapper/save",
					ticketUserMapperEntity, TicketUserMapper.class,
					xyzErrorMessage.TICKETUSERMAPPER_NOT_CREATED.getErrorCode(),
					xyzErrorMessage.TICKETUSERMAPPER_NOT_CREATED.getErrorDetails());

			GuestTicket guestTicket = new GuestTicket();
			guestTicket.setDeviceToken(session.getDeviceToken());
			guestTicket.setTicket(ticketService.getTicketEntityById(ticket.get().getTicketId()));

			RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/guesttickets/save", guestTicket, GuestTicket.class,
					xyzErrorMessage.GUESTTICKET_NOT_CREATED.getErrorCode(),
					xyzErrorMessage.GUESTTICKET_NOT_CREATED.getErrorDetails());

			return new LoginGuestUserResponse(xyzResponseCode.SUCCESS.getCode(),
					xyzResponseMessage.TICKET_USER_ASSOCIATED.getMessage(), session.getSessionToken(),
					session.getRefreshToken(), ticket.get().getTicketId(), ticket.get().getBranchId(),
					ticket.get().getServiceId());
		} else {
			Session session = new Session();
			System.out.println(addGuestTicketPayload.getDeviceToken());
			session.setDeviceToken(addGuestTicketPayload.getDeviceToken());
			session.setIpAddress(null);
			session.setLoginTime(OffsetDateTime.now().withNano(0));
			session.setLogoutTime(null);
			session.setUser(null);

			HttpEntity<Session> entity = new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader());

			session = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/sessions/save", entity, Session.class,
					xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
					xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

			final UserDetails userDetails = new User(TaboorQMSConfigurations.GUEST_EMAIL.getValue(),
					TaboorQMSConfigurations.GUEST_PASSWORD.getValue(), new ArrayList<>());
			session.setSessionToken(jwtTokenProvider.generateToken(userDetails, session.getSessionId()));
			session.setRefreshToken(RandomString.make(40));

			entity = new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader());

			session = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/sessions/save", entity, Session.class,
					xyzErrorMessage.SESSION_NOT_UPDATED.getErrorCode(),
					xyzErrorMessage.SESSION_NOT_UPDATED.getErrorDetails());

			GuestTicket guestTicket = new GuestTicket();
			guestTicket.setDeviceToken(session.getDeviceToken());
			guestTicket.setTicket(ticketService.getTicketEntityById(ticket.get().getTicketId()));

			RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/guesttickets/save", guestTicket, GuestTicket.class,
					xyzErrorMessage.GUESTTICKET_NOT_CREATED.getErrorCode(),
					xyzErrorMessage.GUESTTICKET_NOT_CREATED.getErrorDetails());

			return new LoginGuestUserResponse(xyzResponseCode.SUCCESS.getCode(),
					xyzResponseMessage.GUEST_TICKET_SESSION_CREATED.getMessage(), session.getSessionToken(),
					session.getRefreshToken(), ticket.get().getTicketId(), ticket.get().getBranchId(),
					ticket.get().getServiceId());
		}
	}

	@Override
	public GetGuestUserTicketResponse getKiosikTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
			throws TaboorQMSServiceException, Exception {

		BookCustomerTicketPayload bookGuestTicketPayload = new BookCustomerTicketPayload();
		bookGuestTicketPayload.setBranchId(bookCustomerTicketPayload.getBranchId());
		bookGuestTicketPayload.setServiceId(bookCustomerTicketPayload.getServiceId());
		Ticket guestTicket = ticketService.bookGuestCustomerTicket(bookCustomerTicketPayload);

		return new GetGuestUserTicketResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.GUEST_TICKET_SESSION_CREATED.getMessage(), guestTicket.getTicketNumber());
	}

	@Override
	public List<GetBranchAdminCounterListResponse> getBranchByEmp() throws TaboorQMSServiceException, Exception {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		List<?> getBranchListResponse = RestUtil.get(dbConnectorMicroserviceURL
				+ "/tab/employeebranchmapper/getBranch/user?userId=" + session.getUser().getUserId(), List.class);
		List<Branch> branchList = GenericMapper.convertListToObject(getBranchListResponse,
				new TypeReference<List<Branch>>() {
				});

		List<GetBranchAdminCounterListResponse> branchAdminCounters = new ArrayList<GetBranchAdminCounterListResponse>();

		for (Branch branch : branchList) {
			GetBranchAdminCounterListResponse branchAdminBranch = new GetBranchAdminCounterListResponse();
			branchAdminBranch.setBranchId(branch.getBranchId());
			branchAdminBranch.setBranchName(branch.getBranchName());
			branchAdminBranch.setBranchNameArabic(branch.getBranchNameArabic());
			branchAdminBranch.setEmailAddress(branch.getEmailAddress());

			List<?> getBranchCounterListResponse = RestUtil.get(
					dbConnectorMicroserviceURL + "/tab/branchcounters/get/branchId?branchId=" + branch.getBranchId(),
					List.class);
			List<BranchCounter> branchCounterList = GenericMapper.convertListToObject(getBranchCounterListResponse,
					new TypeReference<List<BranchCounter>>() {
					});

			List<GetBranchAdminCounterListResponse.BranchAdminCounter> branchAdminCounterList = new ArrayList<GetBranchAdminCounterListResponse.BranchAdminCounter>();
			for (BranchCounter branchCounter : branchCounterList) {
				GetBranchAdminCounterListResponse.BranchAdminCounter branchAdminCounter = new GetBranchAdminCounterListResponse().new BranchAdminCounter();
				branchAdminCounter.setCounterId(branchCounter.getCounterId());
				branchAdminCounter.setCounterNumber(branchCounter.getCounterNumber());
				branchAdminCounterList.add(branchAdminCounter);
			}
			branchAdminBranch.setCounterList(branchAdminCounterList);
			branchAdminBranch.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
			branchAdminBranch.setApplicationStatusResponse(xyzResponseMessage.BRANCH_COUNTER_FETCHED.getMessage());
			branchAdminCounters.add(branchAdminBranch);
		}

		return branchAdminCounters;
	}

	@Override
	public GenStatusResponse addCounterConfig(AddCounterConfigPayload addCounterConfigPayload)
			throws TaboorQMSServiceException, Exception {

		BranchCounter branchCounter = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
						+ addCounterConfigPayload.getBranchCounterId(),
				BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

		String uri = dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
				+ branchCounter.getCounterId();
		ResponseEntity<BranchCounterConfig> branchCounterConfigResponse = restTemplate.getForEntity(uri,
				BranchCounterConfig.class);

		if (branchCounterConfigResponse.getBody() != null) {
			throw new TaboorQMSServiceException(xyzErrorMessage.COUNTERCONFIG_ALREADY_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.COUNTERCONFIG_ALREADY_EXISTS.getErrorDetails());
		} else {
			BranchCounterConfig branchCounterConfig = new BranchCounterConfig();
			branchCounterConfig.setBranchCounter(branchCounter);
			branchCounterConfig.setDeviceToken(addCounterConfigPayload.getDeviceToken());
			branchCounterConfig.setFeedBackScreen(addCounterConfigPayload.getFeedBackScreen());

			HttpEntity<BranchCounterConfig> entity = new HttpEntity<>(branchCounterConfig,
					HttpHeadersUtils.getApplicationJsonHeader());
			RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/save", entity,
					BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorCode(),
					xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorDetails());
		}

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage());
	}

	@Override
	public GetTicketCounterResponse getCounterTicket(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception {

		BranchCounter branchCounter = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
						+ getCounterTicketPayload.getBranchCounterId(),
				BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

		Ticket ticket = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/tickets/getTicket/tickerId?branchId="
						+ branchCounter.getBranch().getBranchId() + "&counterNo=" + branchCounter.getCounterNumber(),
				Ticket.class, xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());

		return new GetTicketCounterResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage(), ticket.getTicketNumber(),
				ticket.getCounterNumber());
	}

	// Test Notifications

	@Override
	public GenStatusResponse sendTicketChange(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception {

		BranchCounter branchCounter = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
						+ getCounterTicketPayload.getBranchCounterId(),
				BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

		Random random = new Random();
		String ticketN = String.format("%04d", random.nextInt(10000));

		// send notification to counter application to change the serving ticket

		RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
						+ branchCounter.getCounterId(),
				BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorDetails());

		Map<String, String> metaData = new HashMap<String, String>();
		metaData.put("NotificationCode", PushNotification.TICKET_SERVING_COUNTER.getNotificationCode());
		metaData.put("ticketNumber", "M" + ticketN);
		metaData.put("counterNumber", branchCounter.getCounterNumber());

		NotificationSender.sendPushNotificationData(PushNotification.TICKET_SERVING_COUNTER.getNotificationTitle(),
				PushNotification.TICKET_SERVING_COUNTER.getNotificationDescription(),
				getCounterTicketPayload.getDeviceToken(), metaData);

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage());
	}

	@Override
	public GenStatusResponse sendFeedbackGet(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception {
		BranchCounter branchCounter = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
						+ getCounterTicketPayload.getBranchCounterId(),
				BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

		// send notification to counter application to move to feedback screen
		BranchCounterConfig branchCounterConfig = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
						+ branchCounter.getCounterId(),
				BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorDetails());

		if (branchCounterConfig != null)
			if (branchCounterConfig.getFeedBackScreen()) {
				Map<String, String> metaData = new HashMap<String, String>();
				metaData.put("NotificationCode", PushNotification.TICKET_FEEDBACK.getNotificationCode());
				metaData.put("counterNumber", branchCounter.getCounterNumber());

				NotificationSender.sendPushNotificationData(PushNotification.TICKET_FEEDBACK.getNotificationTitle(),
						PushNotification.TICKET_FEEDBACK.getNotificationDescription(),
						getCounterTicketPayload.getDeviceToken(), metaData);
			}

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage());
	}

}
