package com.taboor.qms.queue.management.model;
// Generated Mar 24, 2020 1:07:58 PM by Hibernate Tools 5.4.7.Final

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_queue")
public class Queue implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long queueId;
	private String queueNumber;
	private LocalTime averageServiceTime;
	private LocalTime averageWaitTime;
	private Long branchId;
	private Long serviceId;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "queue_id", unique = true, nullable = false)
	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

	public String getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(String queueNumber) {
		this.queueNumber = queueNumber;
	}

	public LocalTime getAverageServiceTime() {
		return averageServiceTime;
	}

	public void setAverageServiceTime(LocalTime averageServiceTime) {
		this.averageServiceTime = averageServiceTime;
	}

	public LocalTime getAverageWaitTime() {
		return averageWaitTime;
	}

	public void setAverageWaitTime(LocalTime averageWaitTime) {
		this.averageWaitTime = averageWaitTime;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

}
