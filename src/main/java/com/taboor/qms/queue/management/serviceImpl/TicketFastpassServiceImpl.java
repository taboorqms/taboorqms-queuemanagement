package com.taboor.qms.queue.management.serviceImpl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.payload.CreateUserPaymentTransactionPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.response.CreateAndFundUserPaymentTransactionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetTicketFastpassListResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.PushNotification;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.TicketFastpassStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.core.utils.TransactionType;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.response.GetQueueTicketListResponse.TicketObject;
import com.taboor.qms.queue.management.service.BranchService;
import com.taboor.qms.queue.management.service.QueueService;
import com.taboor.qms.queue.management.service.ServiceTABService;
import com.taboor.qms.queue.management.service.TaboorConfigurationService;
import com.taboor.qms.queue.management.service.TicketFastpassService;
import com.taboor.qms.queue.management.service.TicketService;
import com.taboor.qms.queue.management.service.TransactionService;

@Service
public class TicketFastpassServiceImpl implements TicketFastpassService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Value("${url.microservice.user.management}")
	private String userManagementMicroserviceURL;

	private RestTemplate restTemplate = new RestTemplate();

	@Autowired
	TicketService ticketService;

	@Autowired
	ServiceTABService serviceTABService;

	@Autowired
	BranchService branchService;

	@Autowired
	TaboorConfigurationService taboorConfigurationService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	QueueService queueService;

	@Override
	public TicketFastpass getTicketFastpass(@Valid Long ticketFastpassId) throws TaboorQMSServiceException, Exception {

		TicketFastpass getTicketFastpassByIdResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/ticketfastpass/get/id?ticketFastpassId=" + ticketFastpassId,
				TicketFastpass.class, xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorDetails());

		return getTicketFastpassByIdResponse;
	}

	@Override
	public TicketFastpass getTicketFastpassByTicketId(@Valid Long ticketId)
			throws TaboorQMSServiceException, Exception {

		TicketFastpass getTicketFastpassByIdResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/ticketfastpass/get/ticketId?ticketId=" + ticketId,
				TicketFastpass.class, xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorDetails());

		return getTicketFastpassByIdResponse;
	}

	@Override
	public TicketFastpass checkTicketFastpassByTicketId(@Valid Long ticketId)
			throws TaboorQMSServiceException, Exception {
		TicketFastpass getTicketFastpassByIdResponse = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/ticketfastpass/get/ticketId?ticketId=" + ticketId,
				TicketFastpass.class);
		return getTicketFastpassByIdResponse;
	}

	@Override
	public int deleteTicketFastpass(@Valid Long ticketFastpassId) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/ticketfastpass/delete?ticketFastpassId=" + ticketFastpassId;
		restTemplate = new RestTemplate();
		ResponseEntity<Integer> deleteTicketFastpassResponse = restTemplate.getForEntity(uri, int.class);

		if (!deleteTicketFastpassResponse.getStatusCode().equals(HttpStatus.OK)
				|| deleteTicketFastpassResponse.getBody().equals(0))
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_FASTPASS_NOT_EXISTS.getErrorDetails());

		return 1;
	}

	@Override
	public GetTicketFastpassListResponse getTicketFastpassDetails(@Valid Long ticketFastpassId)
			throws TaboorQMSServiceException, Exception {

		List<TicketFastpass> ticketFastpassList = new ArrayList<TicketFastpass>();
		ticketFastpassList.add(getTicketFastpass(ticketFastpassId));
		return new GetTicketFastpassListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TICKET_FASTPASS_FETCHED.getMessage(), ticketFastpassList);
	}

	@Override
	public GetTicketFastpassListResponse getUserTicketFastpass() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		List<?> getUserTicketFastpassResponse = RestUtil.get(
				dbConnectorMicroserviceURL + "/tab/ticketfastpass/get/user?userId=" + session.getUser().getUserId(),
				List.class);

		List<TicketFastpass> ticketFastpassList = GenericMapper.convertListToObject(getUserTicketFastpassResponse,
				new TypeReference<List<TicketFastpass>>() {
				});

		return new GetTicketFastpassListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_TICKET_FASTPASS_FETCHED.getMessage(), ticketFastpassList);
	}

	@Override
	public TicketFastpass saveTicketFastpass(TicketFastpass ticketFastpass) throws TaboorQMSServiceException {

		HttpEntity<TicketFastpass> ticketFastpassEntity = new HttpEntity<>(ticketFastpass,
				HttpHeadersUtils.getApplicationJsonHeader());

		TicketFastpass createTicketFastpassResponse = RestUtil.postRequest(
				dbConnectorMicroserviceURL + "/tab/ticketfastpass/save", ticketFastpassEntity, TicketFastpass.class,
				xyzErrorMessage.TICKETFASTPASS_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.TICKETFASTPASS_NOT_CREATED.getErrorDetails());

		return createTicketFastpassResponse;
	}

	@Override
	public GenStatusResponse applyTicketFastpass(Long ticketId, Long userBankCardId)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		UserBankCard getUserBankCardResponse = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/userbankcards/get/id?userBankCardId=" + userBankCardId,
				UserBankCard.class, xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorDetails());

		Ticket ticket = ticketService.getTicketById(ticketId);

		if (ticket.getTicketType() == TicketType.FASTPASS.getStatus())
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_ALREADY_FASTPASS.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_ALREADY_FASTPASS.getErrorDetails());

		ServiceCenterConfiguration config = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
						+ branchService.getBranch(ticket.getBranchId()).getServiceCenter().getServiceCenterId(),
				ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

		// Create Transaction
		CreateUserPaymentTransactionPayload createUserPaymentTransactionPayload = new CreateUserPaymentTransactionPayload();
		createUserPaymentTransactionPayload.setAmount(config.getFastpassAmount());
		createUserPaymentTransactionPayload.setExpiryTime(null);
		createUserPaymentTransactionPayload.setTransactionType(TransactionType.FASTPASS_PAYMENT.getStatus());
		createUserPaymentTransactionPayload.setUserBankCard(getUserBankCardResponse);
		createUserPaymentTransactionPayload.setUserBankCardId(getUserBankCardResponse.getBankCardId());

		CreateAndFundUserPaymentTransactionResponse response = transactionService
				.createAndFundFastpassTransaction(createUserPaymentTransactionPayload);
		if (response.getApplicationStatusCode() == xyzResponseCode.ERROR.getCode())
			throw new TaboorQMSServiceException(xyzErrorMessage.TRANSACTION_EXCEPTION_STRIPE.getErrorCode() + "::"
					+ response.getApplicationStatusResponse());

		com.taboor.qms.core.model.Ticket ticketEntity = ticketService.getTicketEntityById(ticket.getTicketId());

		List<TicketObject> ticketList = queueService
				.getSpecificQueueTickets(ticket.getBranchId(), ticket.getServiceId()).getTicketList();
		for (TicketObject t : ticketList)
			if (t.getTicket().getTicketType() == TicketType.FASTPASS.getStatus()) {
				ticketEntity.setPositionTime(t.getTicket().getPositionTime().plusSeconds(1));
				ticket.setPositionTime(t.getTicket().getPositionTime().plusSeconds(1));
			}

		// Create Entry in Ticket Fastpass
		TicketFastpass ticketFastpass = new TicketFastpass();
		ticketFastpass.setApplyTime(OffsetDateTime.now());
		ticketFastpass.setCharges(config.getFastpassAmount());
		ticketFastpass.setStatus(TicketFastpassStatus.CONFIRMED.getStatus());
		ticketFastpass.setTicket(ticketEntity);
		ticketFastpass.setUserPaymentTransaction(response.getUserPaymentTransaction());
		ticketFastpass = saveTicketFastpass(ticketFastpass);

		ticket.setTicketType(TicketType.FASTPASS.getStatus());
		ticket = ticketService.saveTicketH2(ticket);
		ticketService.saveTicket(ticket);

		PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
		userNotificationPayload
				.setTitle(PushNotification.TICKET_FASTPASS_BOOKED.getNotificationTitle() + ticket.getTicketNumber());
		userNotificationPayload.setDescription(PushNotification.TICKET_FASTPASS_BOOKED.getNotificationDescription()+ ticket.getTicketNumber()+ PushNotification.TICKET_FASTPASS_BOOKED_2.getNotificationDescription());

		String uri = userManagementMicroserviceURL + "/notification/push";
		HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
		httpHeaders.setBearerAuth(session.getSessionToken());
		HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(userNotificationPayload,
				httpHeaders);
		ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
				userNotificationEntity, GenStatusResponse.class);

		if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK)
				|| pushNotificationResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode())
			throw new TaboorQMSServiceException(
					444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());

		if (session.getDeviceToken() != null) {
			if (!session.getDeviceToken().isEmpty()) {

				Map<String, String> metaData = new HashMap<String, String>();
				metaData.put("NotificationCode", PushNotification.TICKET_FASTPASS_BOOKED.getNotificationCode());
				metaData.put("ticketNumber", ticket.getTicketNumber());
				metaData.put("ticketId", ticket.getTicketId().toString());
				metaData.put("branchId", ticket.getBranchId().toString());
				metaData.put("serviceId", ticket.getServiceId().toString());
				metaData.put("branchName", branchService.getBranch(ticket.getBranchId()).getBranchName());
				metaData.put("branchAddress", branchService.getBranch(ticket.getBranchId()).getAddress());
				metaData.put("serviceName", serviceTABService.getServiceTAB(ticket.getServiceId()).getServiceName());

				NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
						userNotificationPayload.getDescription(), session.getDeviceToken(), metaData);
			}
		}

		// send notification to all customer apps in queue to indicate new ticket in
		// queue

		com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
				ticket.getServiceId());

		List<?> queueTicketMapperEntityResponse = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/queueticketmapper/get/queueId?queueId=" + queueEntity.getQueueId(),
				List.class);

		List<com.taboor.qms.core.model.QueueTicketMapper> queueTicketMapperEntityList = GenericMapper
				.convertListToObject(queueTicketMapperEntityResponse,
						new TypeReference<List<com.taboor.qms.core.model.QueueTicketMapper>>() {
						});

		for (com.taboor.qms.core.model.QueueTicketMapper qtm : queueTicketMapperEntityList) {

			TicketUserMapper ticketUserMapperEn = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
					+ "/tab/ticketusermapper/get/ticketId?ticketId=" + qtm.getTicket().getTicketId(),
					TicketUserMapper.class);

			if (ticketUserMapperEn != null) {
				if (ticketUserMapperEn.getUser() != null) {
					Session userSession = RestUtil.getRequest(
							dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
									+ ticketUserMapperEn.getUser().getUserId(),
							Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
							xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

					Map<String, String> metaData2 = new HashMap<String, String>();
					metaData2.put("NotificationCode", PushNotification.TICKET_GET_QUEUE.getNotificationCode());
					metaData2.put("ticketNumber", ticketUserMapperEn.getTicket().getTicketNumber());
					metaData2.put("ticketId", ticketUserMapperEn.getTicket().getTicketId().toString());
					metaData2.put("branchId", ticketUserMapperEn.getTicket().getBranch().getBranchId().toString());
					metaData2.put("serviceId", ticketUserMapperEn.getTicket().getService().getServiceId().toString());

					userNotificationPayload.setTitle(
							PushNotification.TICKET_GET_QUEUE.getNotificationTitle() + ticket.getTicketNumber());
					userNotificationPayload
							.setDescription(PushNotification.TICKET_GET_QUEUE.getNotificationDescription());
					userNotificationPayload.setMetaData(metaData2);

					if (userSession.getDeviceToken() != null) {
						if (!userSession.getDeviceToken().isEmpty()) {
							NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
									userNotificationPayload.getDescription(), userSession.getDeviceToken(),
									userNotificationPayload.getMetaData());
						}
					}
				}
			}
		}

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TICKET_FASTPASS_APPLIED.getMessage());
	}

	@Override
	public GenStatusResponse cancelTicketFastpass(long ticketId) throws TaboorQMSServiceException, Exception {
//		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
//				.getPrincipal();
//		Session session = myUserDetails.getSession();
		Ticket ticket = ticketService.getTicketById(ticketId);

		if (ticket.getTicketType() == TicketType.STANDARD.getStatus())
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_ALREADY_STANDARD.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_ALREADY_STANDARD.getErrorDetails());

		TicketFastpass ticketFastpass = getTicketFastpassByTicketId(ticket.getTicketId());

		String uri = dbConnectorMicroserviceURL + "/tab/ticketfastpass/get/transactionId/ticket/latest?ticketId="
				+ ticketId;
		ResponseEntity<Long> getTransactionIdResponse = restTemplate.getForEntity(uri, Long.class);
		if (!getTransactionIdResponse.getStatusCode().equals(HttpStatus.OK) || getTransactionIdResponse.getBody() < 1)
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorCode()
					+ "::" + xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorDetails());

		// Refund Transaction
		GetByIdPayload getByIdPayload = new GetByIdPayload();
		getByIdPayload.setId(getTransactionIdResponse.getBody());

		GenStatusResponse response = transactionService.refundFastpassTransaction(getTransactionIdResponse.getBody());

		if (response.getApplicationStatusCode() == xyzResponseCode.ERROR.getCode())
			throw new TaboorQMSServiceException(xyzErrorMessage.REFUND_EXCEPTION_STRIPE.getErrorCode() + "::"
					+ response.getApplicationStatusResponse());

		// Changes FastPass Status
		ticketFastpass.setStatus(TicketFastpassStatus.CANCELLED.getStatus());
		ticketFastpass = saveTicketFastpass(ticketFastpass);

		ticket.setTicketType(TicketType.STANDARD.getStatus());
		ticket = ticketService.saveTicketH2(ticket);
		ticketService.saveTicket(ticket);

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TICKET_FASTPASS_CANCELLED.getMessage());
	}

}
