package com.taboor.qms.queue.management.response;

import java.util.List;

import com.taboor.qms.core.response.GenStatusResponse;

public class GetTicketEntityListResponse extends GenStatusResponse {

	private List<com.taboor.qms.core.model.Ticket> ticketList;

	public List<com.taboor.qms.core.model.Ticket> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<com.taboor.qms.core.model.Ticket> ticketList) {
		this.ticketList = ticketList;
	}

	public GetTicketEntityListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetTicketEntityListResponse(int applicationStatusCode, String applicationStatusResponse, List<com.taboor.qms.core.model.Ticket> ticketList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticketList = ticketList;
	}

}
