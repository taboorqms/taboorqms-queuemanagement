package com.taboor.qms.queue.management.response;

import com.taboor.qms.core.response.GenStatusResponse;

public class GetTicketCounterResponse extends GenStatusResponse {

	private String ticketNumber;
	private String counterNumber;
	
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getCounterNumber() {
		return counterNumber;
	}
	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}
	
	public GetTicketCounterResponse(int applicationStatusCode, String applicationStatusResponse,
			String ticketNumber, String counterNumber) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticketNumber = ticketNumber;
		this.counterNumber = counterNumber;
	}
}
