package com.taboor.qms.queue.management.serviceImpl;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.queue.management.model.Queue;
import com.taboor.qms.queue.management.model.QueueTicketMapper;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.repository.QueueRepository;
import com.taboor.qms.queue.management.repository.QueueTicketMapperRepository;
import com.taboor.qms.queue.management.response.GetQueueTicketListResponse;
import com.taboor.qms.queue.management.response.GetQueueTicketListResponse.TicketObject;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;
import com.taboor.qms.queue.management.service.BranchService;
import com.taboor.qms.queue.management.service.QueueService;
import com.taboor.qms.queue.management.service.ServiceTABService;

@Service
public class QueueServiceImpl implements QueueService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	ServiceTABService serviceTABService;

	@Autowired
	BranchService branchService;

	@Autowired
	QueueTicketMapperRepository queueTicketMapperRepository;

	@Autowired
	QueueRepository queueRepository;

	RestTemplate restTemplate = new RestTemplate();

	@Override
	public GetTicketEntityListResponse getCustomerPendingTickets() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		List<?> getUserPendingTicketResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
				+ "/tab/queueticketmapper/getTickets/pending/user?userId=" + session.getUser().getUserId(), List.class);

		List<com.taboor.qms.core.model.Ticket> ticketList = new ArrayList<com.taboor.qms.core.model.Ticket>();
		if (getUserPendingTicketResponse != null)
			ticketList = GenericMapper.convertListToObject(getUserPendingTicketResponse,
					new TypeReference<List<com.taboor.qms.core.model.Ticket>>() {
					});

		return new GetTicketEntityListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_PENDING_TICKET_FETCHED.getMessage(), ticketList);
	}

	@Override
	public GetQueueTicketListResponse getSpecificQueueTickets(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception {

		// For H2
		Queue queue = getByBranchAndServiceH2(branchId, serviceId);

		List<QueueTicketMapper> queueTicketMapperList = queueTicketMapperRepository.findByBranchAndService(branchId,
				serviceId);
		List<Ticket> ticketListSorted = new CopyOnWriteArrayList<Ticket>();
		Ticket servingTicket = null;
		for (int i = 0; i < queueTicketMapperList.size(); i++) {
			if (queueTicketMapperList.get(i).getTicket().getTicketStatus() == TicketStatus.SERVING.getStatus())
				servingTicket = queueTicketMapperList.get(i).getTicket();
			ticketListSorted.add(queueTicketMapperList.get(i).getTicket());
		}
		if (servingTicket != null)
			ticketListSorted.remove(servingTicket);
		Collections.sort(ticketListSorted, Comparator.comparing(Ticket::getTransferedStatus)
				.thenComparing(Ticket::getTicketType).thenComparing(Ticket::getPositionTime));
		List<TicketObject> ticketList = new ArrayList<GetQueueTicketListResponse.TicketObject>();
		int j = 1;

		if (servingTicket != null)
			ticketListSorted.add(0, servingTicket);

		for (Ticket ticketObj : ticketListSorted) {

			if (ticketObj.getTicketStatus() == TicketStatus.WAITING.getStatus()) {
				ticketObj.setWaitingTime(
						LocalTime.of(0, 0, 0).plusSeconds(queue.getAverageServiceTime().toSecondOfDay() * (j++)));
				ticketList.add(new TicketObject(ticketObj, null));
			} else if (ticketObj.getCounterNumber() != null) {
				String uri = dbConnectorMicroserviceURL + "/tab/ticketserveds/getAgent/ticket?ticketId="
						+ ticketObj.getTicketId();
				ResponseEntity<Agent> getAgentResponse = restTemplate.getForEntity(uri, Agent.class);
				ticketList.add(new TicketObject(ticketObj, getAgentResponse.getBody()));
			} else {
				ticketList.add(new TicketObject(ticketObj, null));
			}
		}

		return new GetQueueTicketListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.QUEUE_TICKET_FETCHED.getMessage(), queue, ticketList);

	}

	@Override
	public Integer getPositionOfTicketInQueue(Long branchId, Long serviceId, Ticket ticket)
			throws TaboorQMSServiceException, Exception {

		List<QueueTicketMapper> queueTicketMapperList = queueTicketMapperRepository.findByBranchAndService(branchId,
				serviceId);

		List<Ticket> ticketListSorted = new ArrayList<Ticket>();
		for (int i = 0; i < queueTicketMapperList.size(); i++)
			if (queueTicketMapperList.get(i).getTicket().getTicketType() == ticket.getTicketType())
				ticketListSorted.add(queueTicketMapperList.get(i).getTicket());

		Collections.sort(ticketListSorted,
				Comparator.comparing(Ticket::getTransferedStatus).thenComparing(Ticket::getPositionTime));

		return ticketListSorted.indexOf(ticket);
	}

	@Override
	public Queue getByBranchAndServiceH2(Long branchId, Long serviceId) throws TaboorQMSServiceException, Exception {

		Optional<Queue> queue = queueRepository.findByBranchAndServiceAndType(branchId, serviceId);
		if (!queue.isPresent()) {
			com.taboor.qms.core.model.Queue queuePostgres = RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/queues/get/branchAndService?branchId=" + branchId + "&serviceId="
							+ serviceId,
					com.taboor.qms.core.model.Queue.class, xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode(),
					xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
			if (queuePostgres != null) {
				Queue q = new Queue();
				q.setQueueId(queuePostgres.getQueueId());
				q.setAverageServiceTime(queuePostgres.getAverageServiceTime());
				q.setAverageWaitTime(queuePostgres.getAverageWaitTime());
				q.setBranchId(queuePostgres.getBranch().getBranchId());
				q.setQueueNumber(queuePostgres.getQueueNumber());
				q.setServiceId(queuePostgres.getService().getServiceId());
				return q;
			} else
				throw new TaboorQMSServiceException(xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode() + "::"
						+ xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
		}
		return queue.get();
	}

	@Override
	public com.taboor.qms.core.model.Queue getByBranchAndService(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception {

		com.taboor.qms.core.model.Queue queue = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/queues/get/branchAndService?branchId=" + branchId + "&serviceId="
						+ serviceId,
				com.taboor.qms.core.model.Queue.class, xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());

		return queue;
	}

	@Override
	public Queue getByQueueId(Long queueId) throws TaboorQMSServiceException, Exception {

		Optional<Queue> queue = queueRepository.findById(queueId);
		if (!queue.isPresent())
			throw new TaboorQMSServiceException(xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
		return queue.get();
	}

}
