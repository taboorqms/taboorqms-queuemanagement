package com.taboor.qms.queue.management.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GetAllServicesResponse;
import com.taboor.qms.core.response.GetServiceListResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.service.ServiceTABService;

@Service
public class ServiceTABServiceImpl implements ServiceTABService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public ServiceTAB getServiceTAB(@Valid Long serviceId) throws TaboorQMSServiceException, Exception {

		return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/services/get/id?serviceId=" + serviceId,
				ServiceTAB.class, xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
	}

	@Override
	public GetServiceListResponse getServiceTABDetails(@Valid Long serviceId)
			throws TaboorQMSServiceException, Exception {

		List<ServiceTAB> serviceTABList = new ArrayList<ServiceTAB>();
		serviceTABList.add(getServiceTAB(serviceId));
		return new GetServiceListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_FETCHED.getMessage(), serviceTABList);
	}

	@Override
	public GetAllServicesResponse getAllServiceTAB() throws TaboorQMSServiceException, Exception {

		List<?> serviceTABs = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/services/get/all", List.class);
		List<ServiceTAB> serviceList = new ArrayList<ServiceTAB>();
		serviceList = GenericMapper.convertListToObject(serviceTABs, new TypeReference<List<ServiceTAB>>() {
		});

		return new GetAllServicesResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_FETCHED.getMessage(), serviceList);
	}

}
