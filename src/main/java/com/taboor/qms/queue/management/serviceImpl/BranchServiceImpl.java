package com.taboor.qms.queue.management.serviceImpl;


import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.payload.GetNearByBranchPayload;
import com.taboor.qms.core.response.GetBookmarkedBranchListResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchServicesResponse;
import com.taboor.qms.core.utils.BranchStatus;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.response.GetBranchCompleteDetailsResponse;
import com.taboor.qms.queue.management.response.GetBranchCompleteDetailsResponse.BranchServicesCompleteObj;
import com.taboor.qms.queue.management.service.BranchService;
import com.taboor.qms.queue.management.service.ServiceTABService;
@Service
public class BranchServiceImpl implements BranchService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	ServiceTABService serviceTABService;

	@Override
	public Branch getBranch(@Valid Long branchId) throws TaboorQMSServiceException, Exception {
		return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + branchId,
				Branch.class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
	}
	
	@Override
	public GetBranchListResponse getBranchDetails(@Valid Long branchId) throws TaboorQMSServiceException, Exception {
		List<Branch> branchList = new ArrayList<Branch>();
		branchList.add(getBranch(branchId));
		return new GetBranchListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branchList);
	}

	@Override
	public GetBranchServicesResponse getBranchServicesDetails(@Valid Long branchId)
			throws TaboorQMSServiceException, Exception {

		Branch branch = getBranch(branchId);
		List<?> services = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId=" + branchId,
				List.class);
		List<ServiceTAB> serviceList = new ArrayList<ServiceTAB>();
		if (services != null) {
		
			serviceList = GenericMapper.convertListToObject(services, new TypeReference<List<ServiceTAB>>() {
			});
			System.out.println(serviceList);
			for(int i = 0; i < serviceList.size(); i++) {
//				System.out.println(services.get(i).get("serviceId"));
//				System.out.println(map);
				
				for(int j = i; j < serviceList.size(); j++) {

					if(serviceList.get(j).getServiceId() < serviceList.get(i).getServiceId()) {
						System.out.println("Swapping arrays "+j+" and" + i);
						ServiceTAB temp = serviceList.get(i);
						serviceList.set(i, serviceList.get(j));
						serviceList.set(j, temp);
					}
				}
			}

			System.out.println(serviceList);
		}
		List<?> branchWorkingHours = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branchId, List.class);
	
		List<BranchWorkingHours> branchWorkingHoursList = new ArrayList<BranchWorkingHours>();
		branchWorkingHoursList = GenericMapper.convertListToObject(branchWorkingHours,
				new TypeReference<List<BranchWorkingHours>>() {
				});

		branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
		OffsetDateTime temp = OffsetDateTime.now().withNano(0);
		for (BranchWorkingHours item : branchWorkingHoursList) {
			if (item.getDay() == temp.getDayOfWeek().getValue()) {
				if (LocalTime.from(temp).isBefore(item.getClosingTime())
						& LocalTime.from(temp).isAfter(item.getOpeningTime())) {
					System.out.println("Setting branch status open");
					branch.setBranchStatus(BranchStatus.OPEN.getStatus());
				}
			}
		}
		return new GetBranchServicesResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_SERVICES_FETCHED.getMessage(), branch, serviceList);
	}

	@Override
	public GetBranchCompleteDetailsResponse getBranchCompleteDetails(@Valid Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception {

		// serviceId is Optional '0' for Branch All Services otherwise serviceId

		Branch branch = getBranch(branchId);
		List<?> branchWorkingHours = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branchId, List.class);
		List<BranchWorkingHours> branchWorkingHoursList = new ArrayList<BranchWorkingHours>();
		branchWorkingHoursList = GenericMapper.convertListToObject(branchWorkingHours,
				new TypeReference<List<BranchWorkingHours>>() {
				});

		int countBranchServices = RestUtil.getRequestNoCheck(
				dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getServiceCount/branch?branchId=" + branchId,
				Integer.class);

		List<ServiceTAB> servicesList = new ArrayList<ServiceTAB>();
		BranchServicesCompleteObj branchServicesCompleteObj;
		List<BranchServicesCompleteObj> branchServicesCompleteObjs = new ArrayList<BranchServicesCompleteObj>();

		if (countBranchServices > 0) {
			if (serviceId == 0) {
				List<?> services = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
						+ "/tab/branchservicetabmapper/getService/branch?branchId=" + branchId, List.class);
				servicesList = GenericMapper.convertListToObject(services, new TypeReference<List<ServiceTAB>>() {
				});
			} else if (serviceId > 0) {
				ServiceTAB service = RestUtil.getRequestNoCheck(
						dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branchAndService?branchId="
								+ branchId + "&serviceId=" + serviceId, ServiceTAB.class);
				if (service == null)
					throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode() + "::"
							+ xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorDetails());
				servicesList.add(service);
			}
			for (int i = 0; i < servicesList.size(); i++) {

				branchServicesCompleteObj = new BranchServicesCompleteObj();

				Collection<?> serviceRequiredDocuments = RestUtil.getRequestNoCheck(
					dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/getRequirements/serviceCenterIdAndServiceId"
					+ "?serviceCenterId=" + branch.getServiceCenter().getServiceCenterId() + "&serviceId=" +
							servicesList.get(i).getServiceId(), Collection.class); 
				
				Collection<ServiceRequirement> serviceRequiredDocumentsList = GenericMapper.convertListToObject(
						serviceRequiredDocuments, new TypeReference<Collection<ServiceRequirement>>() {
				});

				branchServicesCompleteObj.setServiceTAB(servicesList.get(i));
				branchServicesCompleteObj.setServiceRequirements(serviceRequiredDocumentsList);
				branchServicesCompleteObjs.add(branchServicesCompleteObj);
			}
		}

		branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
		OffsetDateTime temp = OffsetDateTime.now().withNano(0);
		for (BranchWorkingHours item : branchWorkingHoursList) {
			if (item.getDay() == temp.getDayOfWeek().getValue()) {
				if (LocalTime.from(temp).isBefore(item.getClosingTime())
						& LocalTime.from(temp).isAfter(item.getOpeningTime())) {
					System.out.println("Setting branch status open");
					branch.setBranchStatus(BranchStatus.OPEN.getStatus());
				}
			}
		}
		System.out.println(branch.getBranchStatus());
		return new GetBranchCompleteDetailsResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branch, countBranchServices, branchWorkingHoursList,
				branchServicesCompleteObjs);
	}

	@Override
	public GetBookmarkedBranchListResponse getNearByBranches(@Valid final GetNearByBranchPayload payload)
			throws TaboorQMSServiceException, Exception {

		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		Customer customer = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId=" + session.getUser().getUserId(),
				Customer.class, xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());

		Branch[] getBranchByIdResponse = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/branches/get/nearby",
				payload, Branch[].class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());

		List<Branch> branchList = GenericMapper.convertListToObject(getBranchByIdResponse,
				new TypeReference<List<Branch>>() {
				});

		List<GetBookmarkedBranchListResponse.BookmarkedBranches> bookmarkbranches = new ArrayList<GetBookmarkedBranchListResponse.BookmarkedBranches>();

		for (Branch branch : branchList) {
			List<?> branchWorkingHours = RestUtil.getRequestNoCheck(
					dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branch.getBranchId(), List.class);
		
			List<BranchWorkingHours> branchWorkingHoursList = new ArrayList<BranchWorkingHours>();
			branchWorkingHoursList = GenericMapper.convertListToObject(branchWorkingHours,
					new TypeReference<List<BranchWorkingHours>>() {
					});

			branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
			OffsetDateTime temp = OffsetDateTime.now().withNano(0);
			for (BranchWorkingHours item : branchWorkingHoursList) {
				if (item.getDay() == temp.getDayOfWeek().getValue()) {
					if (LocalTime.from(temp).isBefore(item.getClosingTime())
							& LocalTime.from(temp).isAfter(item.getOpeningTime())) {
						System.out.println("Setting branch status open");
						branch.setBranchStatus(BranchStatus.OPEN.getStatus());
					}
				}
			}
			GetBookmarkedBranchListResponse.BookmarkedBranches bookmarkbranch = new GetBookmarkedBranchListResponse().new BookmarkedBranches();

			bookmarkbranch.setAddress(branch.getAddress());
			bookmarkbranch.setAverageServiceTime(branch.getAverageServiceTime());
			bookmarkbranch.setAverageWaitingTime(branch.getAverageWaitingTime());
			bookmarkbranch.setBranchId(branch.getBranchId());
			bookmarkbranch.setBranchName(branch.getBranchName());
			bookmarkbranch.setBranchStatus(branch.getBranchStatus());
			bookmarkbranch.setCity(branch.getCity());
			bookmarkbranch.setLatitude(branch.getLatitude());
			bookmarkbranch.setLongitude(branch.getLongitude());
			bookmarkbranch.setPhoneNumber(branch.getPhoneNumber());
			bookmarkbranch.setRating(branch.getRating());

			Boolean branchBookmarked = RestUtil
					.get(dbConnectorMicroserviceURL + "/tab/branchbookmarks/check/userAndBranch?userId="
							+ customer.getUser().getUserId() + "&branchId=" + branch.getBranchId(), Boolean.class);

			if (branchBookmarked)
				bookmarkbranch.setBookmarked(true);
			else
				bookmarkbranch.setBookmarked(false);

			bookmarkbranches.add(bookmarkbranch);
		}

		return new GetBookmarkedBranchListResponse(bookmarkbranches, xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage());
	}

}
