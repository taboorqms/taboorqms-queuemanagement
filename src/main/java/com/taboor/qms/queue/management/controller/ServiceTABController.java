package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GetAllServicesResponse;
import com.taboor.qms.core.response.GetServiceListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.queue.management.service.ServiceTABService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/service")
@CrossOrigin(origins = "*")
public class ServiceTABController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceTABController.class);

	@Autowired
	ServiceTABService serviceTABService;

	@ApiOperation(value = "Get ServiceTAB Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "ServiceTAB Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetServiceListResponse getServiceTABDetails(@RequestBody @Valid final Long serviceId)
			throws Exception, Throwable {
		logger.info("Calling Get ServiceTAB Details - API");
		return serviceTABService.getServiceTABDetails(serviceId);
	}

	@ApiOperation(value = "Get All ServiceTAB.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All ServiceTAB fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetAllServicesResponse getAllServices() throws Exception, Throwable {
		logger.info("Calling Get All ServiceTAB Names - API");
		return serviceTABService.getAllServiceTAB();
	}

}
