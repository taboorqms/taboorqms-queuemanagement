package com.taboor.qms.queue.management.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.core.response.GetGuestUserTicketResponse;
import com.taboor.qms.core.response.LoginGuestUserResponse;
import com.taboor.qms.queue.management.payload.AddCounterConfigPayload;
import com.taboor.qms.queue.management.payload.AddGuestTicketPayload;
import com.taboor.qms.queue.management.payload.GetCounterTicketPayload;
import com.taboor.qms.queue.management.response.GetTicketCounterResponse;

@Service
public interface GuestTicketService {

	public LoginGuestUserResponse addGuestTicket(AddGuestTicketPayload addGuestTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse addCounterConfig(AddCounterConfigPayload addCounterConfigPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GetTicketCounterResponse getCounterTicket(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GetGuestUserTicketResponse getKiosikTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public List<GetBranchAdminCounterListResponse> getBranchByEmp() throws TaboorQMSServiceException, Exception;
	
	//Test Notifications (to be removed)
	
	public GenStatusResponse sendTicketChange(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse sendFeedbackGet(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;

}