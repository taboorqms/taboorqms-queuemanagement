package com.taboor.qms.queue.management.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GetServiceCentreListResponse;
import com.taboor.qms.queue.management.service.ServiceCenterService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/servicecenter")
@CrossOrigin(origins = "*")
public class ServiceCenterController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceCenterController.class);

	@Autowired
	ServiceCenterService serviceCenterService;

	@ApiOperation(value = "Get All Service centers.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All Service centers fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetServiceCentreListResponse getAllServicecenters() throws Exception, Throwable {
		logger.info("Calling Get All Servicecenters Names - API");
		return serviceCenterService.getAllServicecenters();
	}

}
