package com.taboor.qms.queue.management;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.QueueTicketMapper;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.repository.QueueRepository;
import com.taboor.qms.queue.management.repository.QueueTicketMapperRepository;
import com.taboor.qms.queue.management.repository.TicketRepository;

@Service
public class H2DataMigration {

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	QueueRepository queueRepository;

	@Autowired
	QueueTicketMapperRepository queueTicketMapperRepository;

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@PersistenceContext
	EntityManager em;

	public void dumpTickets() throws RestClientException, TaboorQMSServiceException {

		List<?> listResponseEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/queues/get/all",
				List.class);
		try {
			if (listResponseEntity != null) {
				List<Queue> queueList = GenericMapper.convertListToObject(listResponseEntity,
						new TypeReference<List<Queue>>() {
						});

				queueList.forEach((q) -> {
					com.taboor.qms.queue.management.model.Queue queue = new com.taboor.qms.queue.management.model.Queue();
					queue.setBranchId(q.getBranch().getBranchId());
					queue.setServiceId(q.getService().getServiceId());
					queue.setAverageServiceTime(q.getAverageServiceTime());
					queue.setAverageWaitTime(q.getAverageWaitTime());
					queue.setQueueNumber(q.getQueueNumber());
					queue.setQueueId(q.getQueueId());
					queueRepository.save(queue);
				});
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		// only tickets which are not cancelled or served already
		listResponseEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/queueticketmapper/get/all",
				List.class);
		try {

			if (listResponseEntity != null) {
				List<QueueTicketMapper> queueTicketMapperList = GenericMapper.convertListToObject(listResponseEntity,
						new TypeReference<List<QueueTicketMapper>>() {
						});

				for (QueueTicketMapper queueTicketMapper : queueTicketMapperList) {
					Ticket ticket = new Ticket();
					ticket.setBranchId(queueTicketMapper.getTicket().getBranch().getBranchId());
					if (queueTicketMapper.getTicket().getTransferedCounter() != null)
						ticket.setTransferedCounterId(
								queueTicketMapper.getTicket().getTransferedCounter().getCounterId());
					else
						ticket.setTransferedCounterId(null);

					ticket.setAgentCallTime(queueTicketMapper.getTicket().getAgentCallTime());
					ticket.setCounterNumber(queueTicketMapper.getTicket().getCounterNumber());
					ticket.setCreatedTime(queueTicketMapper.getTicket().getCreatedTime());
					ticket.setPositionTime(queueTicketMapper.getTicket().getPositionTime());
					ticket.setServiceId(queueTicketMapper.getTicket().getService().getServiceId());
					ticket.setTicketId(queueTicketMapper.getTicket().getTicketId());
					ticket.setTicketNumber(queueTicketMapper.getTicket().getTicketNumber());
					ticket.setTicketStatus(queueTicketMapper.getTicket().getTicketStatus());
					ticket.setTicketType(queueTicketMapper.getTicket().getTicketType());
					ticket.setWaitingTime(queueTicketMapper.getTicket().getWaitingTime());
					ticket.setTransferedStatus(queueTicketMapper.getTicket().getTransferedStatus());
					ticketRepository.save(ticket);

					com.taboor.qms.queue.management.model.Queue queue = new com.taboor.qms.queue.management.model.Queue();
					queue.setBranchId(queueTicketMapper.getQueue().getBranch().getBranchId());
					queue.setServiceId(queueTicketMapper.getQueue().getService().getServiceId());
					queue.setAverageServiceTime(queueTicketMapper.getQueue().getAverageServiceTime());
					queue.setAverageWaitTime(queueTicketMapper.getQueue().getAverageWaitTime());
					queue.setQueueNumber(queueTicketMapper.getQueue().getQueueNumber());
					queue.setQueueId(queueTicketMapper.getQueue().getQueueId());
					queueRepository.save(queue);

					com.taboor.qms.queue.management.model.QueueTicketMapper qtm = new com.taboor.qms.queue.management.model.QueueTicketMapper();
					qtm.setQueue(queue);
					qtm.setTicket(ticket);
					qtm.setQueueTicketId(queueTicketMapper.getQueueTicketId());
					queueTicketMapperRepository.save(qtm);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Success");
	}

	@PostConstruct
	void dataMigrationH2() throws RestClientException, TaboorQMSServiceException {
		dumpTickets();
	}

}
