package com.taboor.qms.queue.management.serviceImpl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.response.GetRequiredDocumentResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.service.BranchService;
import com.taboor.qms.queue.management.service.RequiredDocumentService;

@Service
public class RequiredDocumentServiceImpl implements RequiredDocumentService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	BranchService branchService;

	@Override
	public GetRequiredDocumentResponse getRequiredDocumentByBranchAndService(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception {
		Branch branch = branchService.getBranch(branchId);
		Collection<?> serviceRequiredDocuments = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
				+ "/tab/servicecenterservicetabmapper/getRequirements/serviceCenterIdAndServiceId" + "?serviceCenterId="
				+ branch.getServiceCenter().getServiceCenterId() + "&serviceId=" + serviceId, Collection.class);

		Collection<ServiceRequirement> serviceRequiredDocumentsList = new ArrayList<ServiceRequirement>();

		if(serviceRequiredDocuments!=null)
			serviceRequiredDocumentsList = GenericMapper.convertListToObject(serviceRequiredDocuments,
				new TypeReference<Collection<ServiceRequirement>>() {
				});

		return new GetRequiredDocumentResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_SERVICE_DOCUMENT_FETCHED.getMessage(), serviceRequiredDocumentsList);
	}

}
