package com.taboor.qms.queue.management.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.response.GetRequiredDocumentResponse;

@Service
public interface RequiredDocumentService {

	GetRequiredDocumentResponse getRequiredDocumentByBranchAndService(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception;

}
