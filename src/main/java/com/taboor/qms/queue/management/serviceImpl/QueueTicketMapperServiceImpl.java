package com.taboor.qms.queue.management.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.queue.management.model.QueueTicketMapper;
import com.taboor.qms.queue.management.repository.QueueTicketMapperRepository;
import com.taboor.qms.queue.management.service.QueueTicketMapperService;

@Service
public class QueueTicketMapperServiceImpl implements QueueTicketMapperService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Autowired
	QueueTicketMapperRepository queueTicketMapperRepository;

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public com.taboor.qms.core.model.QueueTicketMapper saveQueueTicketMapper(
			com.taboor.qms.core.model.QueueTicketMapper queueTicketMapper) throws TaboorQMSServiceException, Exception {

		HttpEntity<com.taboor.qms.core.model.QueueTicketMapper> queueticketMapperEntity = new HttpEntity<>(
				queueTicketMapper, HttpHeadersUtils.getApplicationJsonHeader());
		com.taboor.qms.core.model.QueueTicketMapper queueTicketMapperResponse = RestUtil.postRequestEntity(
				dbConnectorMicroserviceURL + "/tab/queueticketmapper/save", queueticketMapperEntity,
				com.taboor.qms.core.model.QueueTicketMapper.class,
				xyzErrorMessage.QUEUETICKETMAPPER_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.QUEUETICKETMAPPER_NOT_CREATED.getErrorDetails());
		return queueTicketMapperResponse;
	}

	@Override
	public QueueTicketMapper getByTicketId(Long ticketId) throws TaboorQMSServiceException, Exception {

		Optional<QueueTicketMapper> entity = queueTicketMapperRepository.findByTicketId(ticketId);
		if (!entity.isPresent())
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorDetails());
		return entity.get();
	}

	@Override
	public int deleteById(Long queueTicketMapperId) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/delete?queueTicketMapperId="
				+ queueTicketMapperId;
		restTemplate = new RestTemplate();
		ResponseEntity<Integer> deleteQueueTicketMapperResponse = restTemplate.getForEntity(uri, int.class);

		if (!deleteQueueTicketMapperResponse.getStatusCode().equals(HttpStatus.OK)
				|| deleteQueueTicketMapperResponse.getBody().equals(0))
			throw new TaboorQMSServiceException(xyzErrorMessage.QUEUETICKETMAPPER_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.QUEUETICKETMAPPER_NOT_DELETED.getErrorDetails());
		// H2
		queueTicketMapperRepository.deleteById(queueTicketMapperId);
		return 1;
	}

	@Override
	public int deleteTicketByTicketId(Long ticketId) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/delete/ticketId?ticketId=" + ticketId;
		ResponseEntity<Integer> deleteQueueTicketResponse = restTemplate.getForEntity(uri, Integer.class);
		if (!deleteQueueTicketResponse.getStatusCode().equals(HttpStatus.OK)
				|| deleteQueueTicketResponse.getBody() == 0)
			throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorDetails());

		// H2
		queueTicketMapperRepository.deleteByTicketId(ticketId);
		return 1;
	}

	@Override
	public int counterQueueFastpassTickets(Long queueId) throws TaboorQMSServiceException, Exception {
		return queueTicketMapperRepository.countQueueFastpassTicketsByQueueId(queueId, TicketType.FASTPASS.getStatus());
	}

	@Override
	public QueueTicketMapper saveQueueTicketMapperH2(QueueTicketMapper queueTicketMapper)
			throws TaboorQMSServiceException, Exception {
		// H2
		return queueTicketMapperRepository.save(queueTicketMapper);
	}

}
