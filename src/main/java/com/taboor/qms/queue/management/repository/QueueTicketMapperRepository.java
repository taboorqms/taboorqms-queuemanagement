package com.taboor.qms.queue.management.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.queue.management.model.QueueTicketMapper;

@Repository
@Transactional
public interface QueueTicketMapperRepository extends JpaRepository<QueueTicketMapper, Long> {

	@Query("SELECT q FROM QueueTicketMapper q where qtm_ticket_id = :ticketId")
	Optional<QueueTicketMapper> findByTicketId(@Param("ticketId") Long ticketId);

	@Modifying
	@Query("DELETE from QueueTicketMapper qtm where queue_ticket_id = :queueTicketId")
	int deleteByQueueTicketId(@Param("queueTicketId") Long queueTicketId);

	@Modifying
	@Query("DELETE from QueueTicketMapper qtm where qtm_ticket_id = :ticketId")
	int deleteByTicketId(@Param("ticketId") Long ticketId);

	@Query("SELECT count(qtm.queueTicketId) FROM QueueTicketMapper qtm inner join Queue q"
			+ " on q.queueId=qtm.queue inner join Ticket t on t.ticketId=qtm.ticket where q.queueId=:queueId"
			+ " and t.ticketType=:fastpassTypeId")
	int countQueueFastpassTicketsByQueueId(@Param("queueId") long queueId, @Param("fastpassTypeId") int fastpassTypeId);

	@Query("SELECT qtm FROM QueueTicketMapper qtm inner join Queue q on qtm_queue_id=q.queueId where q.branchId =:branchId and q.serviceId =:serviceId")
	List<QueueTicketMapper> findByBranchAndService(@Param("branchId") Long branchId,
			@Param("serviceId") Long serviceId);

	@Query("SELECT count(qtm.queueTicketId) FROM QueueTicketMapper qtm inner join Queue q"
			+ " on qtm_queue_id = q.queueId inner join Ticket t on t.ticketId = qtm.ticket"
			+ " where q.branchId =:branchId and q.serviceId =:serviceId and t.ticketType != 3 and t.ticketStatus != 3 and ( t.transferedCounterId IS NULL or t.transferedCounterId = :counterId )")
	int countQueueTicketsNotTransferedByCounterId(@Param("branchId") Long branchId, @Param("serviceId") Long serviceId,
			@Param("counterId") Long counterId);
}
