package com.taboor.qms.queue.management.response;

import java.util.Collection;
import java.util.List;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetBranchCompleteDetailsResponse extends GenStatusResponse {

	private Branch branch;
	private int servicesCount;
	private List<BranchServicesCompleteObj> branchServicesCompleteObjs;
	private List<BranchWorkingHours> branchWorkingHourList;

	public static class BranchServicesCompleteObj {

		private ServiceTAB serviceTAB;
		private Collection<ServiceRequirement> serviceRequirements;

		public ServiceTAB getServiceTAB() {
			return serviceTAB;
		}

		public void setServiceTAB(ServiceTAB serviceTAB) {
			this.serviceTAB = serviceTAB;
		}

		public Collection<ServiceRequirement> getServiceRequirements() {
			return serviceRequirements;
		}

		public void setServiceRequirements(Collection<ServiceRequirement> serviceRequirements) {
			this.serviceRequirements = serviceRequirements;
		}

	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public List<BranchServicesCompleteObj> getBranchServicesCompleteObjs() {
		return branchServicesCompleteObjs;
	}

	public void setBranchServicesCompleteObjs(List<BranchServicesCompleteObj> branchServicesCompleteObjs) {
		this.branchServicesCompleteObjs = branchServicesCompleteObjs;
	}

	public int getServicesCount() {
		return servicesCount;
	}

	public void setServicesCount(int servicesCount) {
		this.servicesCount = servicesCount;
	}

	public List<BranchWorkingHours> getBranchWorkingHourList() {
		return branchWorkingHourList;
	}

	public void setBranchWorkingHourList(List<BranchWorkingHours> branchWorkingHourList) {
		this.branchWorkingHourList = branchWorkingHourList;
	}

	public GetBranchCompleteDetailsResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetBranchCompleteDetailsResponse(int applicationStatusCode, String applicationStatusResponse, Branch branch,
			int servicesCount, List<BranchWorkingHours> branchWorkingHourList,
			List<BranchServicesCompleteObj> branchServicesCompleteObjs) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branch = branch;
		this.servicesCount = servicesCount;
		this.branchWorkingHourList = branchWorkingHourList;
		this.branchServicesCompleteObjs = branchServicesCompleteObjs;
	}

}
