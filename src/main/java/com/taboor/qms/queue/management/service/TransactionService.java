package com.taboor.qms.queue.management.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.UserPaymentTransaction;
import com.taboor.qms.core.payload.CreateUserPaymentTransactionPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.CreateAndFundUserPaymentTransactionResponse;
import com.taboor.qms.core.response.GenStatusResponse;

@Service
public interface TransactionService {

	UserPaymentTransaction saveUserPaymentTransaction(@Valid UserPaymentTransaction userPaymentTransaction)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse fundTransaction(@Valid GetByIdPayload getByIdPayload) throws TaboorQMSServiceException, Exception;

	CreateAndFundUserPaymentTransactionResponse createAndFundFastpassTransaction(
			@Valid CreateUserPaymentTransactionPayload createUserPaymentTransactionPayload)
			throws TaboorQMSServiceException, Exception, AuthenticationException, InvalidRequestException,
	        APIConnectionException, CardException, APIException;

	GenStatusResponse refundFastpassTransaction(long userPaymentTransactionId)
			throws TaboorQMSServiceException, Exception;

}
