package com.taboor.qms.queue.management.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetTicketFastpassListResponse;

@Service
public interface TicketFastpassService {

	public TicketFastpass getTicketFastpass(@Valid Long ticketFastpassId) throws TaboorQMSServiceException, Exception;
	
	public TicketFastpass checkTicketFastpassByTicketId(@Valid Long ticketId) throws TaboorQMSServiceException, Exception;
	
	public TicketFastpass saveTicketFastpass(TicketFastpass ticketFastpass) throws TaboorQMSServiceException;

	public int deleteTicketFastpass(@Valid Long ticketFastpassId) throws TaboorQMSServiceException, Exception;

	public TicketFastpass getTicketFastpassByTicketId(@Valid Long ticketId) throws TaboorQMSServiceException, Exception;

	public GetTicketFastpassListResponse getTicketFastpassDetails(@Valid Long ticketFastpassId)
			throws TaboorQMSServiceException, Exception;

	public GetTicketFastpassListResponse getUserTicketFastpass()
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse applyTicketFastpass(Long ticketId, Long userBankCardId)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse cancelTicketFastpass(long ticketId)
			throws TaboorQMSServiceException, Exception;
}
