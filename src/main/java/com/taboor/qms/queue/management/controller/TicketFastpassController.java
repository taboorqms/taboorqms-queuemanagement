package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetTicketFastpassListResponse;
import com.taboor.qms.queue.management.service.TicketFastpassService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/ticketFastpass")
@CrossOrigin(origins = "*")
public class TicketFastpassController {

	private static final Logger logger = LoggerFactory.getLogger(TicketFastpassController.class);

	@Autowired
	TicketFastpassService ticketFastpassService;

	@ApiOperation(value = "Get TicketFastpass Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "TicketFastpass Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketFastpassListResponse getTicketFastpassDetails(
			@RequestBody @Valid final Long ticketFastpassId) throws Exception, Throwable {
		logger.info("Calling Get TicketFastpass Details - API");
		return ticketFastpassService.getTicketFastpassDetails(ticketFastpassId);
	}

	@ApiOperation(value = "Get User Ticket FastPass.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Ticket FastPass fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetTicketFastpassListResponse getUserTicketFastpass() throws Exception, Throwable {
		logger.info("Calling Get User Ticket FastPass - API");
		return ticketFastpassService.getUserTicketFastpass();
	}

	@ApiOperation(value = "Apply Ticket Fastpass.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket Fastpass applied Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fastpass/apply", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse transferTicketToFastpass(
			@RequestBody @Valid GetByIdListPayload getByIdListPayload) throws Exception, Throwable {
		logger.info("Calling Apply Ticket Fastpass - API");
		// 0-ticketId, 1-UserBankCardId
		return ticketFastpassService.applyTicketFastpass(getByIdListPayload.getIds().get(0),
				getByIdListPayload.getIds().get(1));
	}

	@ApiOperation(value = "Cancel ticket Fastpass.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket Fastpass cancelled Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fastpass/cancel", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse cancelTicketFastpass(@RequestBody @Valid GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Cancel ticket Fastpass - API");
		// 0-ticketId
		return ticketFastpassService.cancelTicketFastpass(getByIdPayload.getId());
	}

}
