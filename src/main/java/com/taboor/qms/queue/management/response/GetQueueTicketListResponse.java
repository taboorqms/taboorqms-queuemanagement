package com.taboor.qms.queue.management.response;

import java.util.List;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.queue.management.model.Queue;
import com.taboor.qms.queue.management.model.Ticket;

public class GetQueueTicketListResponse extends GenStatusResponse {

	private Queue queueInfo;
	private List<TicketObject> ticketList;

	public static class TicketObject {
		private Ticket ticket;
		private Agent agent;

		public Ticket getTicket() {
			return ticket;
		}

		public void setTicket(Ticket ticket) {
			this.ticket = ticket;
		}

		public Agent getAgent() {
			return agent;
		}

		public void setAgent(Agent agent) {
			this.agent = agent;
		}

		public TicketObject(Ticket ticket, Agent agent) {
			super();
			this.ticket = ticket;
			this.agent = agent;
			if (this.agent != null) {
				this.agent.setBranch(null);
				this.agent.getServiceCenterEmployee().setServiceCenter(null);
			}
		}
		
		public TicketObject() {
			
		}
	}

	public GetQueueTicketListResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Queue getQueueInfo() {
		return queueInfo;
	}

	public void setQueueInfo(Queue queueInfo) {
		this.queueInfo = queueInfo;
	}

	public List<TicketObject> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<TicketObject> ticketList) {
		this.ticketList = ticketList;
	}

	public GetQueueTicketListResponse(int applicationStatusCode, String applicationStatusResponse, Queue queueInfo,
			List<TicketObject> ticketList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.queueInfo = queueInfo;
//		this.queueInfo.setBranchId(null);
		this.ticketList = ticketList;
	}

}
