package com.taboor.qms.queue.management.serviceImpl;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Token;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.model.UserPaymentTransaction;
import com.taboor.qms.core.payload.CreateUserPaymentTransactionPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.CreateAndFundUserPaymentTransactionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.utils.Currency;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.core.utils.TransactionStatus;
import com.taboor.qms.core.utils.TransactionType;
import com.taboor.qms.queue.management.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	private RestTemplate restTemplate = new RestTemplate();

	@PostConstruct
	public void init() {
		Stripe.apiKey = TaboorQMSConfigurations.STRIPE_SECRET_KEY.getValue();
	}

	@Override
	public UserPaymentTransaction saveUserPaymentTransaction(@Valid UserPaymentTransaction userPaymentTransaction)
			throws TaboorQMSServiceException, Exception {

		String uri = dbConnectorMicroserviceURL + "/tab/userpaymenttransactions/save";
		HttpEntity<UserPaymentTransaction> entity = new HttpEntity<>(userPaymentTransaction,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserPaymentTransaction> createUserPaymentTransactionResponse = restTemplate.postForEntity(uri,
				entity, UserPaymentTransaction.class);

		if (!createUserPaymentTransactionResponse.getStatusCode().equals(HttpStatus.OK)
				|| createUserPaymentTransactionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERPAYMENTTRANSACTION_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERPAYMENTTRANSACTION_NOT_CREATED.getErrorDetails());

		return createUserPaymentTransactionResponse.getBody();

	}

	@Override
	public GenStatusResponse fundTransaction(@Valid GetByIdPayload getByIdPayload)
			throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/userpaymenttransactions/get/id?userPaymentTransactionId="
				+ getByIdPayload.getId();
		ResponseEntity<UserPaymentTransaction> getUserPaymentTransactionResponse = restTemplate.getForEntity(uri,
				UserPaymentTransaction.class);

		if (!getUserPaymentTransactionResponse.getStatusCode().equals(HttpStatus.OK)
				|| getUserPaymentTransactionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorCode()
					+ "::" + xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorDetails());

		UserPaymentTransaction userPaymentTransaction = getUserPaymentTransactionResponse.getBody();

		if (userPaymentTransaction.getExpiryTime().isBefore(OffsetDateTime.now()))
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_PAYMENT_TRANSACTION_EXPIRED.getErrorCode() + "::"
					+ xyzErrorMessage.USER_PAYMENT_TRANSACTION_EXPIRED.getErrorDetails());

		Token token;
		Charge transactionResponse = null;

		if (!userPaymentTransaction.getUserBankCard().getCardType().equals("Taboor Credit")) {
			Map<String, Object> card = new HashMap<>();
			card.put("number", userPaymentTransaction.getUserBankCard().getCardNumber());
			card.put("exp_month", userPaymentTransaction.getUserBankCard().getExpiryMonth());
			card.put("exp_year", userPaymentTransaction.getUserBankCard().getExpiryYear());
			card.put("cvc", userPaymentTransaction.getUserBankCard().getCardCvv());
			card.put("name", userPaymentTransaction.getUserBankCard().getUser().getEmail());
			Map<String, Object> param = new HashMap<>();
			param.put("card", card);

			try {
				token = Token.create(param);
				Double amount = userPaymentTransaction.getAmount() * 100;
				Map<String, Object> chargeParams = new HashMap<>();
				chargeParams.put("amount", Math.round(amount.intValue()));
				chargeParams.put("currency", Currency.AED);
				chargeParams.put("description", "Fastpass payment");
				chargeParams.put("source", token.getId());
				transactionResponse = Charge.create(chargeParams);
			} catch (CardException e) {
				throw new TaboorQMSServiceException(
						xyzErrorMessage.USER_BANK_CARD_EXCEPTION_STRIPE.getErrorCode() + "::" + e.getMessage());
			} catch (RateLimitException e) {
				throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
			} catch (InvalidRequestException e) {
				throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
			} catch (AuthenticationException e) {
				throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
			} catch (APIConnectionException e) {
				throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
			} catch (StripeException e) {
				throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
			}

			userPaymentTransaction.setStripetransactionId(transactionResponse.getId());
			userPaymentTransaction.setCompletedTime(OffsetDateTime.now());
			userPaymentTransaction.setTransactionStatus(TransactionStatus.PAID.getStatus());
		} else {
			if (userPaymentTransaction.getUserBankCard().getBalance() < userPaymentTransaction.getAmount())
				throw new TaboorQMSServiceException(xyzErrorMessage.INSUFFICIENT_BALANCE.getErrorCode() + "::"
						+ xyzErrorMessage.INSUFFICIENT_BALANCE.getErrorDetails());

			// TODO Detect Balance
			userPaymentTransaction.getUserBankCard().setBalance(
					userPaymentTransaction.getUserBankCard().getBalance() - userPaymentTransaction.getAmount());

			userPaymentTransaction.setStripetransactionId(null);
			userPaymentTransaction.setCompletedTime(OffsetDateTime.now());
			userPaymentTransaction.setTransactionStatus(TransactionStatus.PAID.getStatus());

			uri = dbConnectorMicroserviceURL + "/tab/userbankcards/save";
			HttpEntity<UserBankCard> entity = new HttpEntity<>(userPaymentTransaction.getUserBankCard(),
					HttpHeadersUtils.getApplicationJsonHeader());
			ResponseEntity<UserBankCard> updateUserBankCardResponse = restTemplate.postForEntity(uri, entity,
					UserBankCard.class);

			if (!updateUserBankCardResponse.getStatusCode().equals(HttpStatus.OK)
					|| updateUserBankCardResponse.getBody() == null)
				throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode() + "::"
						+ xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());
		}

		saveUserPaymentTransaction(userPaymentTransaction);

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TRANSACTION_FUNDED.getMessage());
	}

	@Override
	public CreateAndFundUserPaymentTransactionResponse createAndFundFastpassTransaction(
			@Valid CreateUserPaymentTransactionPayload createUserPaymentTransactionPayload)
			throws TaboorQMSServiceException, Exception, AuthenticationException, InvalidRequestException,
			APIConnectionException, CardException, APIException {

		UserPaymentTransaction userPaymentTransaction = new UserPaymentTransaction();
		userPaymentTransaction.setAmount(createUserPaymentTransactionPayload.getAmount());
		userPaymentTransaction.setCompletedTime(null);
		userPaymentTransaction.setCreatedTime(OffsetDateTime.now());
		userPaymentTransaction.setExpiryTime(OffsetDateTime.now().plusMinutes(10));
		userPaymentTransaction
				.setPaymentMethod(createUserPaymentTransactionPayload.getUserBankCard().getPaymentMethod());
		userPaymentTransaction.setTransactionStatus(TransactionStatus.PENDING.getStatus());
		userPaymentTransaction.setTransactionType(createUserPaymentTransactionPayload.getTransactionType());
		userPaymentTransaction.setUserBankCard(createUserPaymentTransactionPayload.getUserBankCard());

		userPaymentTransaction = saveUserPaymentTransaction(userPaymentTransaction);

		GetByIdPayload getByIdPayload = new GetByIdPayload();
		getByIdPayload.setId(userPaymentTransaction.getTransactionId());

		GenStatusResponse genStatusResponse = fundTransaction(getByIdPayload);

		if (genStatusResponse.getApplicationStatusCode() == xyzResponseCode.ERROR.getCode())
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_FUNDED.getErrorCode()
					+ "::" + xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_FUNDED.getErrorDetails());

		return new CreateAndFundUserPaymentTransactionResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TRANSACTION_CREATED_AND_FUNDED.getMessage(), userPaymentTransaction);
	}

	@Override
	public GenStatusResponse refundFastpassTransaction(long userPaymentTransactionId)
			throws TaboorQMSServiceException, Exception {

		String uri = dbConnectorMicroserviceURL + "/tab/userpaymenttransactions/get/id?userPaymentTransactionId="
				+ userPaymentTransactionId;
		ResponseEntity<UserPaymentTransaction> getUserPaymentTransactionResponse = restTemplate.getForEntity(uri,
				UserPaymentTransaction.class);

		if (!getUserPaymentTransactionResponse.getStatusCode().equals(HttpStatus.OK)
				|| getUserPaymentTransactionResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorCode()
					+ "::" + xyzErrorMessage.USER_PAYMENT_TRANSACTION_NOT_EXISTS.getErrorDetails());

		UserPaymentTransaction userPaymentTransaction = getUserPaymentTransactionResponse.getBody();

		uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/userAndPaymentMethodId?userId="
				+ userPaymentTransaction.getUserBankCard().getUser().getUserId() + "&paymentMethodId=1";
		// PaymentMethodId is 1 for Taboor Credit

		ResponseEntity<UserBankCard> getUserBankCardResponse = restTemplate.getForEntity(uri, UserBankCard.class);

		if (!getUserBankCardResponse.getStatusCode().equals(HttpStatus.OK) || getUserBankCardResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USER_TABOOR_CREDIT_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.USER_TABOOR_CREDIT_NOT_EXISTS.getErrorDetails());

		UserBankCard userBankCard = getUserBankCardResponse.getBody();
		userBankCard.setBalance(userBankCard.getBalance() + userPaymentTransaction.getAmount());

		uri = dbConnectorMicroserviceURL + "/tab/userbankcards/save";
		HttpEntity<UserBankCard> entity = new HttpEntity<>(userBankCard, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserBankCard> updateUserBankCardResponse = restTemplate.postForEntity(uri, entity,
				UserBankCard.class);

		if (!updateUserBankCardResponse.getStatusCode().equals(HttpStatus.OK)
				|| updateUserBankCardResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());

//		Map<String, Object> params = new HashMap<>();
//        params.put(
//          "charge",
//          userPaymentTransaction.getStripetransactionId()
//        );
//        
//        Refund refund;
//        
//        try {
//        	refund = Refund.create(params);
//	   	} catch (CardException e) {
//	   		throw new TaboorQMSServiceException(xyzErrorMessage.USER_BANK_CARD_EXCEPTION_STRIPE.getErrorCode() + "::" + e.getMessage());
//	   	} catch (RateLimitException e) {
//	   		throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
//	   	} catch (InvalidRequestException e) {
//	   		throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
//	   	} catch (AuthenticationException e) {
//	   		throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
//	   	} catch (APIConnectionException e) {
//	   		throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
//	   	} catch (StripeException e) {
//	   		throw new TaboorQMSServiceException(e.getStatusCode() + "::" + e.getMessage());
//	   	}

		UserPaymentTransaction refundPaymentTransaction = new UserPaymentTransaction();
		refundPaymentTransaction.setAmount(userPaymentTransaction.getAmount());
		refundPaymentTransaction.setCompletedTime(OffsetDateTime.now());
		refundPaymentTransaction.setCreatedTime(OffsetDateTime.now());
		refundPaymentTransaction.setExpiryTime(null);
		refundPaymentTransaction.setPaymentMethod(userPaymentTransaction.getPaymentMethod());
		refundPaymentTransaction.setTransactionStatus(TransactionStatus.PAID.getStatus());
		refundPaymentTransaction.setTransactionType(TransactionType.FASTPASS_REFUND.getStatus());
		refundPaymentTransaction.setUserBankCard(userPaymentTransaction.getUserBankCard());

		refundPaymentTransaction = saveUserPaymentTransaction(refundPaymentTransaction);

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TRANSACTION_FASTPASS_REFUNDED.getMessage());
	}

}
