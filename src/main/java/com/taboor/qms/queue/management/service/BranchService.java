package com.taboor.qms.queue.management.service;


import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.payload.GetNearByBranchPayload;
import com.taboor.qms.core.response.GetBookmarkedBranchListResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchServicesResponse;
import com.taboor.qms.queue.management.response.GetBranchCompleteDetailsResponse;

@Service
public interface BranchService {

	public Branch getBranch(@Valid Long branchId) throws TaboorQMSServiceException, Exception;



	public GetBranchListResponse getBranchDetails(@Valid Long branchId) throws TaboorQMSServiceException, Exception;

	public GetBranchServicesResponse getBranchServicesDetails(@Valid Long branchId)
			throws TaboorQMSServiceException, Exception;

	public GetBranchCompleteDetailsResponse getBranchCompleteDetails(@Valid Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception;

	public GetBookmarkedBranchListResponse getNearByBranches(@Valid GetNearByBranchPayload payload) throws TaboorQMSServiceException, Exception;
}