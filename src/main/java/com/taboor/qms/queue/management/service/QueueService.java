package com.taboor.qms.queue.management.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.queue.management.model.Queue;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.response.GetQueueTicketListResponse;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;

@Service
public interface QueueService {

	Queue getByQueueId(Long queueId) throws TaboorQMSServiceException, Exception;

	GetTicketEntityListResponse getCustomerPendingTickets() throws TaboorQMSServiceException, Exception;

	Queue getByBranchAndServiceH2(Long branchId, Long serviceId) throws TaboorQMSServiceException, Exception;

	com.taboor.qms.core.model.Queue getByBranchAndService(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception;

	GetQueueTicketListResponse getSpecificQueueTickets(Long branchId, Long serviceId)
			throws TaboorQMSServiceException, Exception;
	
	Integer getPositionOfTicketInQueue(Long branchId, Long serviceId, Ticket ticket)
			throws TaboorQMSServiceException, Exception;
}
