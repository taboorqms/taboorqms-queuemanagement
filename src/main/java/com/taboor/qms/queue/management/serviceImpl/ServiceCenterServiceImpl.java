package com.taboor.qms.queue.management.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.response.GetServiceCentreListResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.service.ServiceCenterService;

@Service
public class ServiceCenterServiceImpl implements ServiceCenterService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GetServiceCentreListResponse getAllServicecenters() throws TaboorQMSServiceException, Exception {

		List<?> serviceCenters = RestUtil.get(dbConnectorMicroserviceURL + "/tab/servicecenters/get/all", List.class);
		List<ServiceCenter> serviceCenterList = new ArrayList<ServiceCenter>();
		serviceCenterList = GenericMapper.convertListToObject(serviceCenters, new TypeReference<List<ServiceCenter>>() {
		});
		List<GetServiceCentreListResponse.ServiceCentreResponse> serviceCentreResponseList = new ArrayList<GetServiceCentreListResponse.ServiceCentreResponse>();

		for (ServiceCenter serviceCenter : serviceCenterList) {
			GetServiceCentreListResponse.ServiceCentreResponse serviceCentreResponse = new GetServiceCentreListResponse().new ServiceCentreResponse();
			serviceCentreResponse.setServiceCenterId(serviceCenter.getServiceCenterId());
			serviceCentreResponse.setServiceCenterName(serviceCenter.getServiceCenterName());
			serviceCentreResponse.setCountry(serviceCenter.getCountry());
			serviceCentreResponse.setServiceCenterNameArabic(serviceCenter.getServiceCenterArabicName());

			int countBranches = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
					+ "/tab/branches/count/serviceCenterId?serviceCenterId=" + serviceCenter.getServiceCenterId(), Integer.class);

			serviceCentreResponse.setNumberOfBranches(countBranches);
			serviceCentreResponseList.add(serviceCentreResponse);
		}

		return new GetServiceCentreListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_CENTER_FETCHED.getMessage(), serviceCentreResponseList);
	}

}
