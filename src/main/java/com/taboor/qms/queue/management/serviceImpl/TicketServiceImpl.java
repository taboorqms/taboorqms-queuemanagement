package com.taboor.qms.queue.management.serviceImpl;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.AgentCounterConfig;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.core.model.BranchCounterConfig;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.core.model.BranchTVConfig;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.GuestTicket;
import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.TicketFeedback;
import com.taboor.qms.core.model.TicketServed;
import com.taboor.qms.core.model.TicketStepOut;
import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.payload.AddTicketFeedbackCounterPayload;
import com.taboor.qms.core.payload.AddTicketFeedbackPayload;
import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.payload.ServeTicketPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.BranchStatus;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.PushNotification;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.ServiceReviewStatus;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.core.utils.TicketTransferedStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.queue.management.TaboorQmsQueueManagementApplication;
import com.taboor.qms.queue.management.model.Queue;
import com.taboor.qms.queue.management.model.QueueTicketMapper;
import com.taboor.qms.queue.management.model.Ticket;
import com.taboor.qms.queue.management.repository.QueueRepository;
import com.taboor.qms.queue.management.repository.QueueTicketMapperRepository;
import com.taboor.qms.queue.management.repository.TicketRepository;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;
import com.taboor.qms.queue.management.response.GetTicketListResponse;
import com.taboor.qms.queue.management.service.BranchService;
import com.taboor.qms.queue.management.service.QueueService;
import com.taboor.qms.queue.management.service.QueueTicketMapperService;
import com.taboor.qms.queue.management.service.ServiceTABService;
import com.taboor.qms.queue.management.service.TaboorConfigurationService;
import com.taboor.qms.queue.management.service.TicketFastpassService;
import com.taboor.qms.queue.management.service.TicketService;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TicketServiceImpl implements TicketService {

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    private RestTemplate restTemplate = new RestTemplate();

    private static DecimalFormat df = new DecimalFormat("0.00");

    @Autowired
    ServiceTABService serviceTABService;

    @Autowired
    BranchService branchService;

    @Autowired
    QueueService queueService;

    @Autowired
    TicketFastpassService ticketFastpassService;

    @Autowired
    QueueTicketMapperService queueTicketMapperService;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    QueueRepository queueRepository;

    @Autowired
    QueueTicketMapperRepository queueTicketMapperRepository;

    @Autowired
    TaboorConfigurationService taboorConfigurationService;

    private static final Logger logger = LoggerFactory.getLogger(TicketServiceImpl.class);

    private Customer findCustomerByUserId(Long userId) throws TaboorQMSServiceException {
        Customer getByUserEmailResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/customers/get/userId?userId=" + userId, Customer.class,
                xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());
        return getByUserEmailResponse;
    }

    @Override
    public Ticket saveTicketH2(Ticket ticket) throws TaboorQMSServiceException, Exception {

        // H2
        Ticket ticketResponse = ticketRepository.save(ticket);
        return ticketResponse;
    }

    @Override
    public com.taboor.qms.core.model.Ticket saveTicket(Ticket ticket) throws TaboorQMSServiceException, Exception {

        com.taboor.qms.core.model.Ticket ticketEntity = null;
        if (ticket.getTicketId() != null) {
            ticketEntity = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/tickets/get/id?ticketId=" + ticket.getTicketId(),
                    com.taboor.qms.core.model.Ticket.class);
        }

        if (ticketEntity == null) {
            ticketEntity = new com.taboor.qms.core.model.Ticket();
        }

        Branch branch = new Branch();
        branch.setBranchId(ticket.getBranchId());
        ticketEntity.setBranch(branch);
        ticketEntity.setAgentCallTime(ticket.getAgentCallTime());
        ticketEntity.setCounterNumber(ticket.getCounterNumber());
        ticketEntity.setCreatedTime(ticket.getCreatedTime());
        ticketEntity.setPositionTime(ticket.getPositionTime());
        ServiceTAB serviceTAB = new ServiceTAB();
        serviceTAB.setServiceId(ticket.getServiceId());
        ticketEntity.setService(serviceTAB);
        ticketEntity.setTicketNumber(ticket.getTicketNumber());
        ticketEntity.setTicketStatus(ticket.getTicketStatus());
        ticketEntity.setTicketType(ticket.getTicketType());
        ticketEntity.setWaitingTime(ticket.getWaitingTime());
        ticketEntity.setTransferedStatus(ticket.getTransferedStatus());
        ticketEntity.setTicketId(ticket.getTicketId());
        ticketEntity.setTransferedCounter(null);
        ticketEntity.setStepOut(ticket.getStepOut() == null ? false : ticket.getStepOut());
        ticketEntity.setStepOutPosition(ticket.getStepOutPosition());

        HttpEntity<com.taboor.qms.core.model.Ticket> ticketEntityResponse = new HttpEntity<>(ticketEntity,
                HttpHeadersUtils.getApplicationJsonHeader());
        com.taboor.qms.core.model.Ticket createTicketResponse = RestUtil.postRequestEntity(
                dbConnectorMicroserviceURL + "/tab/tickets/save", ticketEntityResponse,
                com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.TICKET_NOT_CREATED.getErrorDetails());
        return createTicketResponse;
    }

    @Override
    public TicketStepOut saveTicketStepOut(TicketStepOut ticketStepOut) throws TaboorQMSServiceException, Exception {
        TicketStepOut saveTicketStepOutResponse = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/ticketstepouts/save", ticketStepOut, TicketStepOut.class,
                xyzErrorMessage.TICKETSTEPOUT_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.TICKETSTEPOUT_NOT_CREATED.getErrorDetails());
        ticketStepOut = saveTicketStepOutResponse;
        return ticketStepOut;
    }

    @Override
    public Ticket getTicketById(Long ticketId) throws TaboorQMSServiceException, Exception {
        Optional<com.taboor.qms.queue.management.model.Ticket> ticket = ticketRepository.findById(ticketId);
        return ticket.get();
    }

    @Override
    public com.taboor.qms.core.model.Ticket getTicketEntityById(Long ticketId)
            throws TaboorQMSServiceException, Exception {
        com.taboor.qms.core.model.Ticket getTicketResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/tickets/get/id?ticketId=" + ticketId,
                com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());
        if (getTicketResponse.getTicketType() == TicketType.CANCELED.getStatus() || getTicketResponse.getTicketType() == TicketType.EXPIRED.getStatus()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());
        }
        return getTicketResponse;
    }

    @Override
    public TicketStepOut getStepOutTicketById(Long ticketId) throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/ticketstepouts/get/ticket?ticketId=" + ticketId;
        ResponseEntity<TicketStepOut> getTicketStepOutResponse = restTemplate.getForEntity(uri, TicketStepOut.class);
        if (!getTicketStepOutResponse.getStatusCode().equals(HttpStatus.OK)
                || getTicketStepOutResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_STEPOUT_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_STEPOUT_NOT_EXISTS.getErrorDetails());
        }
        return getTicketStepOutResponse.getBody();
    }

    public TicketStepOut getStepOutTicketById(com.taboor.qms.queue.management.model.Ticket ticket)
            throws TaboorQMSServiceException, Exception {

        TicketStepOut checkStepOutTicketResponse = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketstepouts/get/ticket?ticketId=" + ticket.getTicketId(),
                TicketStepOut.class);
        return checkStepOutTicketResponse;
    }

    @Override
    public GetTicketListResponse getTicketByIdRest(Long ticketId) throws TaboorQMSServiceException, Exception {
        List<com.taboor.qms.queue.management.model.Ticket> ticketList = new ArrayList<com.taboor.qms.queue.management.model.Ticket>();
        ticketList.add(getTicketById(ticketId));
        return new GetTicketListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_FETCHED.getMessage(), ticketList);
    }

    @Override
    public GenStatusResponse bookCustomerTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        Branch branch = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + bookCustomerTicketPayload.getBranchId(),
                Branch.class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());

        int inQueueTickets = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/inQueue?userId="
                + session.getUser().getUserId(),
                Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        System.out.println(inQueueTickets);

        if (inQueueTickets > 0) {
            throw new TaboorQMSServiceException(444 + "::" + "Sorry, You've already Purchased a ticket.");
        }

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/get/serviceCenterIdAndServiceId?"
                + "serviceCenterId=" + branch.getServiceCenter().getServiceCenterId() + "&serviceId="
                + bookCustomerTicketPayload.getServiceId(),
                ServiceCenterServiceTABMapper.class, xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorDetails());

        if (serviceCenterServiceTABMapper.getKiosikEnabled() == null) {
            serviceCenterServiceTABMapper.setKiosikEnabled(false);
        }

        if (serviceCenterServiceTABMapper.getKiosikEnabled()) {
            throw new TaboorQMSServiceException(444 + "::" + "Sorry, Ticket of service '"
                    + serviceCenterServiceTABMapper.getServiceTAB().getServiceName() + "' can booked from KIOSK Only");
        }

        Long ticketNumber = RestUtil.get(dbConnectorMicroserviceURL + "/tab/tickets/get/number", Long.class);

        Customer customer = findCustomerByUserId(session.getUser().getUserId());

        Ticket ticket = new Ticket();
        ticket.setTicketStatus(TicketStatus.BOOKED.getStatus());
        ticket.setTicketType(TicketType.STANDARD.getStatus());
        ticket.setCreatedTime(OffsetDateTime.now().withNano(0));
        ticket.setPositionTime(ticket.getCreatedTime());
        ticket.setBranchId(bookCustomerTicketPayload.getBranchId());
        ticket.setServiceId(bookCustomerTicketPayload.getServiceId());
        ticket.setServiceName(serviceCenterServiceTABMapper.getServiceTAB().getServiceName());
        ticket.setStepOut(false);

        ticket.setTicketNumber(branch.getBranchName().substring(0, 1).toUpperCase() + String.valueOf(ticketNumber));
        ticket.setTransferedStatus(TicketTransferedStatus.NOTTRANSFERED.getStatus());

        com.taboor.qms.core.model.Ticket entity = saveTicket(ticket);
        ticket.setTicketId(entity.getTicketId());
        ticket = saveTicketH2(ticket);

        TicketUserMapper ticketUserMapper = new TicketUserMapper();
        ticketUserMapper.setTicket(entity);
        ticketUserMapper.setUser(customer.getUser());

        String uri = dbConnectorMicroserviceURL + "/tab/ticketusermapper/save";
        HttpEntity<TicketUserMapper> ticketUserMapperEntity = new HttpEntity<>(ticketUserMapper,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<TicketUserMapper> createTicketUserMapperResponse = restTemplate.postForEntity(uri,
                ticketUserMapperEntity, TicketUserMapper.class);

        if (!createTicketUserMapperResponse.getStatusCode().equals(HttpStatus.OK)
                || createTicketUserMapperResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKETUSERMAPPER_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.TICKETUSERMAPPER_NOT_CREATED.getErrorDetails());
        }

        com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
                ticket.getServiceId());

        Queue queueEn = new Queue();
        Optional<Queue> queue = queueRepository.findByBranchAndServiceAndType(ticket.getBranchId(),
                ticket.getServiceId());
        if (!queue.isPresent()) {
            queueEn.setAverageServiceTime(queueEntity.getAverageServiceTime());
            queueEn.setAverageWaitTime(queueEntity.getAverageWaitTime());
            queueEn.setBranchId(ticket.getBranchId());
            queueEn.setServiceId(ticket.getServiceId());
            queueEn.setQueueNumber(queueEntity.getQueueNumber());
            queueEn.setQueueId(queueEntity.getQueueId());
            queueRepository.save(queueEn);
        }

        QueueTicketMapper queueTicketMapper = new QueueTicketMapper();
        queueTicketMapper.setQueue(queue.isPresent() ? queue.get() : queueEn);
        queueTicketMapper.setTicket(ticket);

        com.taboor.qms.core.model.QueueTicketMapper queueTicketMapperEntity = new com.taboor.qms.core.model.QueueTicketMapper();
        queueTicketMapperEntity.setQueue(queueEntity);
        queueTicketMapperEntity.setTicket(entity);

        com.taboor.qms.core.model.QueueTicketMapper qtmEntity = queueTicketMapperService
                .saveQueueTicketMapper(queueTicketMapperEntity);
        queueTicketMapper.setQueueTicketId(qtmEntity.getQueueTicketId());
        queueTicketMapper = queueTicketMapperService.saveQueueTicketMapperH2(queueTicketMapper);

        ticket.setTicketStatus(TicketStatus.WAITING.getStatus());
        saveTicket(ticket);
        ticket = saveTicketH2(ticket);

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.TICKET_BOOKED_1.getNotificationCode());
        metaData.put("ticketNumber", ticket.getTicketNumber());
        metaData.put("ticketId", ticket.getTicketId().toString());
        metaData.put("branchId", ticket.getBranchId().toString());
        metaData.put("serviceId", ticket.getServiceId().toString());
        metaData.put("branchName", branchService.getBranch(ticket.getBranchId()).getBranchName());
        metaData.put("branchAddress", branchService.getBranch(ticket.getBranchId()).getAddress());
        metaData.put("serviceName", serviceTABService.getServiceTAB(ticket.getServiceId()).getServiceName());

        PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
        userNotificationPayload
                .setTitle(PushNotification.TICKET_BOOKED_1.getNotificationTitle() + ticket.getTicketNumber());
        userNotificationPayload.setDescription(PushNotification.TICKET_BOOKED_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_BOOKED_2.getNotificationDescription());
        userNotificationPayload.setMetaData(metaData);

        uri = userManagementMicroserviceURL + "/notification/push";
        HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
        httpHeaders.setBearerAuth(session.getSessionToken());
        HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(userNotificationPayload,
                httpHeaders);
        ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                userNotificationEntity, GenStatusResponse.class);

        if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK)
                || pushNotificationResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
            throw new TaboorQMSServiceException(
                    444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
        }

        sendNotificationCustomerApps(queueEntity, ticket);

        // sending notification to agent app to notify that ticket is available in Queue
        // now..
        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        for (BranchCounter counter : counterList) {

            List<?> branchCounterServiceTABs = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                    + counter.getCounterId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                    branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
            });

            int sumTicketsWaitingInQueue = 0;

            for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                int ticketsWaitingInQueue = getNoOfTicketsInQueue(ticket.getBranchId(),
                        bcst.getServiceTAB().getServiceId(), counter.getCounterId());
                sumTicketsWaitingInQueue += ticketsWaitingInQueue;
            }

            AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + counter.getCounterId(),
                    AgentCounterConfig.class);

            if (agentCounterConfigEntity != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));
                System.out.println("ticketsWaitingInQueue Notification No --->> " + sumTicketsWaitingInQueue);
                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfigEntity.getDeviceToken(), metaData1);
            }
        }

        /////// end notification code
        if (session.getDeviceToken() != null) {
            if (!session.getDeviceToken().isEmpty()) {
                NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                        userNotificationPayload.getDescription(), session.getDeviceToken(), metaData);
            }
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.TICKET_BOOKED.getMessage());
    }

    @Override
    public Ticket bookGuestCustomerTicket(@Valid BookCustomerTicketPayload bookCustomerTicketPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceTAB serviceTAB = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/services/get/id?serviceId="
                + bookCustomerTicketPayload.getServiceId(),
                ServiceTAB.class, xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorDetails());

        Long ticketNumber = RestUtil.get(dbConnectorMicroserviceURL + "/tab/tickets/get/number", Long.class);

        Ticket ticket = new Ticket();
        ticket.setTicketStatus(TicketStatus.BOOKED.getStatus());
        ticket.setTicketType(TicketType.STANDARD.getStatus());
        ticket.setCreatedTime(OffsetDateTime.now().withNano(0));
        ticket.setPositionTime(ticket.getCreatedTime());
        ticket.setBranchId(bookCustomerTicketPayload.getBranchId());
        ticket.setServiceId(bookCustomerTicketPayload.getServiceId());
        ticket.setServiceName(serviceTAB.getServiceName());
        ticket.setStepOut(false);
        ticket.setTicketNumber(
                branchService.getBranch(ticket.getBranchId()).getBranchName().substring(0, 1).toUpperCase()
                + String.valueOf(ticketNumber));
        ticket.setTransferedStatus(TicketTransferedStatus.NOTTRANSFERED.getStatus());

        com.taboor.qms.core.model.Ticket ticketEntity = saveTicket(ticket);
        ticket.setTicketId(ticketEntity.getTicketId());
        ticket = saveTicketH2(ticket);

//		Queue queue = queueService.getByBranchAndServiceH2(ticket.getBranchId(), ticket.getServiceId());
        com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
                ticket.getServiceId());

        Queue queueEn = new Queue();
        Optional<Queue> queue = queueRepository.findByBranchAndServiceAndType(ticket.getBranchId(),
                ticket.getServiceId());
        if (!queue.isPresent()) {
            queueEn.setAverageServiceTime(queueEntity.getAverageServiceTime());
            queueEn.setAverageWaitTime(queueEntity.getAverageWaitTime());
            queueEn.setBranchId(ticket.getBranchId());
            queueEn.setServiceId(ticket.getServiceId());
            queueEn.setQueueNumber(queueEntity.getQueueNumber());
            queueEn.setQueueId(queueEntity.getQueueId());
            queueRepository.save(queueEn);
        }

        com.taboor.qms.core.model.QueueTicketMapper queueTicketMapperEnitity = new com.taboor.qms.core.model.QueueTicketMapper();
        queueTicketMapperEnitity.setQueue(queueEntity);
        queueTicketMapperEnitity.setTicket(ticketEntity);
        queueTicketMapperEnitity = queueTicketMapperService.saveQueueTicketMapper(queueTicketMapperEnitity);

        QueueTicketMapper queueTicketMapper = new QueueTicketMapper();
        queueTicketMapper.setQueue(queue.isPresent() ? queue.get() : queueEn);
        queueTicketMapper.setTicket(ticket);
        queueTicketMapper.setQueueTicketId(queueTicketMapperEnitity.getQueueTicketId());
        queueTicketMapper = queueTicketMapperService.saveQueueTicketMapperH2(queueTicketMapper);

        ticket.setTicketStatus(TicketStatus.WAITING.getStatus());
        saveTicket(ticket);
        ticket = saveTicketH2(ticket);

        if (session != null) {

            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.TICKET_BOOKED_1.getNotificationCode());
            metaData.put("ticketNumber", ticket.getTicketNumber());
            metaData.put("ticketId", ticket.getTicketId().toString());
            metaData.put("branchId", ticket.getBranchId().toString());
            metaData.put("serviceId", ticket.getServiceId().toString());
            metaData.put("branchName", branchService.getBranch(ticket.getBranchId()).getBranchName());
            metaData.put("branchAddress", branchService.getBranch(ticket.getBranchId()).getAddress());
            metaData.put("serviceName", serviceTABService.getServiceTAB(ticket.getServiceId()).getServiceName());

            PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
            userNotificationPayload
                    .setTitle(PushNotification.TICKET_BOOKED_1.getNotificationTitle() + ticket.getTicketNumber());
            userNotificationPayload.setDescription(PushNotification.TICKET_BOOKED_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_BOOKED_2.getNotificationDescription());
            userNotificationPayload.setMetaData(metaData);

            String uri = userManagementMicroserviceURL + "/notification/push";
            HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
            httpHeaders.setBearerAuth(session.getSessionToken());
            HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(userNotificationPayload,
                    httpHeaders);
            ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                    userNotificationEntity, GenStatusResponse.class);

            if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK)
                    || pushNotificationResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                throw new TaboorQMSServiceException(
                        444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
            }

            // sending notification to agent app to notify that ticket is available in Queue
            // now..
            List<?> branchCounterList = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                    + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                    new TypeReference<List<BranchCounter>>() {
            });

            for (BranchCounter counter : counterList) {
                List<?> branchCounterServiceTABs = RestUtil.getRequest(
                        dbConnectorMicroserviceURL
                        + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                        + counter.getCounterId(),
                        List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

                List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                        branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
                });

                int sumTicketsWaitingInQueue = 0;

                for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                    int ticketsWaitingInQueue = getNoOfTicketsInQueue(ticket.getBranchId(),
                            bcst.getServiceTAB().getServiceId(), counter.getCounterId());
                    sumTicketsWaitingInQueue += ticketsWaitingInQueue;
                }

                AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                        + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + counter.getCounterId(),
                        AgentCounterConfig.class);

                if (agentCounterConfigEntity != null) {
                    Map<String, String> metaData1 = new HashMap<String, String>();
                    metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                    metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));
                    System.out.println("ticketsWaitingInQueue Notification No --->> " + sumTicketsWaitingInQueue);
                    NotificationSender.sendPushNotificationData(
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                            agentCounterConfigEntity.getDeviceToken(), metaData1);
                }
            }

            /////// end notification code
            if (session.getDeviceToken() != null) {
                if (!session.getDeviceToken().isEmpty()) {

                    System.out.println("In condition --->> session.getDeviceToken() != null");

                    ticketEntity = getTicketEntityById(ticket.getTicketId());
                    NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                            userNotificationPayload.getDescription(), session.getDeviceToken(), metaData);
                }
            }
        }

        // send notification to all customer apps in queue to indicate new ticket in
        // queue
        sendNotificationCustomerApps(queueEntity, ticket);

        return ticket;
    }

    @Override
    public int deleteById(Long ticketId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/tickets/delete?ticketId=" + ticketId;
        restTemplate = new RestTemplate();
        ResponseEntity<Integer> deleteTicketResponse = restTemplate.getForEntity(uri, int.class);

        if (!deleteTicketResponse.getStatusCode().equals(HttpStatus.OK) || deleteTicketResponse.getBody().equals(0)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_NOT_DELETED.getErrorDetails());
        }
        // H2
        ticketRepository.deleteByTicketId(ticketId);
        return 1;
    }

    @Override
    public GetTicketEntityListResponse transferTicket(Long ticketId, Long newBranchId)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        Branch branch = branchService.getBranch(newBranchId);
        com.taboor.qms.queue.management.model.Ticket ticket = getTicketById(ticketId);
        GetTicketEntityListResponse response;
        List<com.taboor.qms.core.model.Ticket> ticketList = new ArrayList<com.taboor.qms.core.model.Ticket>();

        RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branchAndService?branchId="
                + newBranchId + "&serviceId=" + ticket.getServiceId(),
                ServiceTAB.class, xyzErrorMessage.BRANCH_SERVICE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_SERVICE_NOT_EXISTS.getErrorDetails());

        TicketFastpass ticketFastpass = ticketFastpassService.checkTicketFastpassByTicketId(ticket.getTicketId());

        if (ticketFastpass != null) {
            StepInOutTicketPayload payload = new StepInOutTicketPayload();
            payload.setTicketId(ticketId);
            cancelCustomerTicket(payload);
        }

        if (deleteById(ticket.getTicketId()) == 1) {
            BookCustomerTicketPayload bookCustomerTicketPayload = new BookCustomerTicketPayload();
            bookCustomerTicketPayload.setBranchId(branch.getBranchId());
            bookCustomerTicketPayload.setServiceId(ticket.getServiceId());
            if (bookCustomerTicket(bookCustomerTicketPayload).getApplicationStatusCode() == xyzResponseCode.SUCCESS
                    .getCode()) {
                response = new GetTicketEntityListResponse();
            }
            response = queueService.getCustomerPendingTickets();

            if (ticketFastpass != null) {
                // PaymentMethodId is 1 for Taboor Credit
                UserBankCard userBankCard = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/userbankcards/get/userAndPaymentMethodId?userId="
                        + session.getUser().getUserId() + "&paymentMethodId=1",
                        UserBankCard.class, xyzErrorMessage.USER_TABOOR_CREDIT_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.USER_TABOOR_CREDIT_NOT_EXISTS.getErrorDetails());

                ticketFastpassService.applyTicketFastpass(response.getTicketList().get(0).getTicketId(),
                        userBankCard.getBankCardId());
            }

            ticketList.add(response.getTicketList().get(0));

            return new GetTicketEntityListResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.TICKET_TRANSFERRED.getMessage(), ticketList);
        }

        return new GetTicketEntityListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_TRANSFERRED.getMessage(), ticketList);
    }

    @Override
    public GetValuesResposne getTicketFastpassEstimation(long queueId) throws TaboorQMSServiceException, Exception {

        Queue queue = queueService.getByQueueId(queueId);
        int queueFastpassTicketCount = queueTicketMapperService.counterQueueFastpassTickets(queueId);
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("EstimatedPosition", String.valueOf(queueFastpassTicketCount + 1));
        values.put("EstimatedWaitingTime", LocalTime
                .ofSecondOfDay(queue.getAverageServiceTime().toSecondOfDay() * queueFastpassTicketCount).toString());
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_FASTPASS_ESTIMATED.getMessage(), values);
    }

    @Override
    public GetValuesResposne stepOutCustomerTicket(@Valid StepInOutTicketPayload stepOutTicketPayload)
            throws TaboorQMSServiceException, Exception {

        Ticket ticket = getTicketById(stepOutTicketPayload.getTicketId());
        int position = queueService.getPositionOfTicketInQueue(ticket.getBranchId(), ticket.getServiceId(), ticket) + 1;
        TicketStepOut ticketStepOutRecord = getStepOutTicketById(ticket);

        ServiceCenterConfiguration config = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + branchService.getBranch(ticket.getBranchId()).getServiceCenter().getServiceCenterId(),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

        if (ticketStepOutRecord != null) {

            ticketStepOutRecord.setStepOutCount(ticketStepOutRecord.getStepOutCount() + 1);

            if (ticketStepOutRecord.getTicket().getTicketType() == TicketType.FASTPASS.getStatus()
                    && ticketStepOutRecord.getStepOutCount() > config.getFastpassStepoutAllowedCount()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_STEP_OUT_LIMITEXCEEDED.getErrorCode() + "::"
                        + xyzErrorMessage.TICKET_STEP_OUT_LIMITEXCEEDED.getErrorDetails());
            } else if (ticketStepOutRecord.getTicket().getTicketType() == TicketType.STANDARD.getStatus()
                    && ticketStepOutRecord.getStepOutCount() > config.getNormalStepoutAllowedCount()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_STEP_OUT_LIMITEXCEEDED.getErrorCode() + "::"
                        + xyzErrorMessage.TICKET_STEP_OUT_LIMITEXCEEDED.getErrorDetails());
            } else if (ticketStepOutRecord.getTicket().getTicketType() == TicketType.STEPOUT.getStatus()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_STEP_OUT_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.TICKET_STEP_OUT_EXISTS.getErrorDetails());
            }

            ticketStepOutRecord.setStepOutTime(OffsetDateTime.now());
            ticket.setStepOut(true);
            ticket.setStepOutPosition(position);
            ticket.setTicketType(TicketType.STEPOUT.getStatus());
            saveTicket(ticket);
            ticket = saveTicketH2(ticket);
            saveTicketStepOut(ticketStepOutRecord);

        } else {

            ticket.setStepOut(true);
            ticket.setStepOutPosition(position);

            TicketStepOut ticketStepOut = new TicketStepOut();

            if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
                ticketStepOut.setStepInTime(
                        OffsetDateTime.now().plusMinutes(Long.valueOf(config.getFastpassStepoutTime().getMinute())));
            } else {
                ticketStepOut.setStepInTime(
                        OffsetDateTime.now().plusMinutes(Long.valueOf(config.getNormalStepoutTime().getMinute())));
            }

            ticket.setTicketType(TicketType.STEPOUT.getStatus());
            com.taboor.qms.core.model.Ticket ticketEntity = saveTicket(ticket);
            ticket = saveTicketH2(ticket);

            ticketStepOut.setTicket(ticketEntity);
            ticketStepOut.setStepOutTime(OffsetDateTime.now());
            ticketStepOut.setStepOutCount(1);
            saveTicketStepOut(ticketStepOut);
        }

        // send notification to all customer apps in queue to indicate new ticket in
        // queue
        com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
                ticket.getServiceId());
        sendNotificationCustomerApps(queueEntity, ticket);

        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        for (BranchCounter counter : counterList) {

            List<?> branchCounterServiceTABs = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                    + counter.getCounterId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                    branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
            });

            int sumTicketsWaitingInQueue = 0;

            for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                int ticketsWaitingInQueue = getNoOfTicketsInQueue(ticket.getBranchId(),
                        bcst.getServiceTAB().getServiceId(), counter.getCounterId());
                sumTicketsWaitingInQueue += ticketsWaitingInQueue;
            }

            AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + counter.getCounterId(),
                    AgentCounterConfig.class);

            if (agentCounterConfigEntity != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));
                System.out.println("ticketsWaitingInQueue Notification No --->> " + sumTicketsWaitingInQueue);
                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfigEntity.getDeviceToken(), metaData1);
            }
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("PrevoiusPosition", position);
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.TICKET_STEPOUT.getMessage(),
                values);
    }

    @Override
    public GenStatusResponse stepInCustomerTicket(@Valid StepInOutTicketPayload stepInTicketPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        boolean cancelTicket = false;

        Ticket ticket = getTicketById(stepInTicketPayload.getTicketId());
        TicketStepOut ticketStepOut = getStepOutTicketById(stepInTicketPayload.getTicketId());
        TicketFastpass ticketFastpass = ticketFastpassService
                .checkTicketFastpassByTicketId(stepInTicketPayload.getTicketId());

        ServiceCenterConfiguration config = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + branchService.getBranch(ticket.getBranchId()).getServiceCenter().getServiceCenterId(),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

        com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
                ticket.getServiceId());
        sendNotificationCustomerApps(queueEntity, ticket);

        if (ticketFastpass != null) {
            if (OffsetDateTime.now().minusMinutes(Long.valueOf(config.getFastpassStepoutTime().getMinute()))
                    .compareTo(ticketStepOut.getStepOutTime()) == 0
                    || OffsetDateTime.now().minusMinutes(Long.valueOf(config.getFastpassStepoutTime().getMinute()))
                            .compareTo(ticketStepOut.getStepOutTime()) == -1) {
                ticket.setTicketType(TicketType.FASTPASS.getStatus());
                ticketStepOut.setStepInTime(OffsetDateTime.now());
                ticket.setPositionTime(getMedianOfTickets(ticket.getBranchId(), ticket.getServiceId(),
                        TicketType.FASTPASS, ticket.getStepOutPosition()));
                ticket.setStepOut(false);
                ticket.setStepOutPosition(0);
                System.out.println("Time: " + ticket.getPositionTime());

                saveTicket(ticket);
                saveTicketH2(ticket);
                saveTicketStepOut(ticketStepOut);

            } else {
                // cancel ticket
                double balance = ticketFastpass.getCharges();
                ticketFastpass.setCharges(0.0);

                String uri = dbConnectorMicroserviceURL + "/tab/paymentmethods/get/name?paymentMethod="
                        + "Taboor Credit";
                ResponseEntity<PaymentMethod> getPaymentMethodResponse = restTemplate.getForEntity(uri,
                        PaymentMethod.class);
                if (!getPaymentMethodResponse.getStatusCode().equals(HttpStatus.OK)
                        || getPaymentMethodResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorCode() + "::"
                            + xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorDetails());
                }

                uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/userAndPaymentMethodId?userId="
                        + session.getUser().getUserId() + "&paymentMethodId="
                        + getPaymentMethodResponse.getBody().getPaymentMethodId();
                ResponseEntity<UserBankCard> getBankCardResponse = restTemplate.getForEntity(uri, UserBankCard.class);
                if (!getBankCardResponse.getStatusCode().equals(HttpStatus.OK) || getBankCardResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorCode() + "::"
                            + xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorDetails());
                }

                // TODO
                getBankCardResponse.getBody().setBalance(getBankCardResponse.getBody().getBalance() + balance);

                uri = dbConnectorMicroserviceURL + "/tab/userbankcards/save";
                ResponseEntity<UserBankCard> saveBankCardResponse = restTemplate.postForEntity(uri,
                        getBankCardResponse.getBody(), UserBankCard.class);
                if (!saveBankCardResponse.getStatusCode().equals(HttpStatus.OK)
                        || saveBankCardResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode() + "::"
                            + xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());
                }
                ticketFastpassService.saveTicketFastpass(ticketFastpass);
                cancelTicket = true;
            }
        } else {

            if (OffsetDateTime.now().minusMinutes(Long.valueOf(config.getNormalStepoutTime().getMinute()))
                    .compareTo(ticketStepOut.getStepOutTime()) == 0
                    || OffsetDateTime.now().minusMinutes(Long.valueOf(config.getNormalStepoutTime().getMinute()))
                            .compareTo(ticketStepOut.getStepOutTime()) == -1) {
                ticket.setTicketType(TicketType.STANDARD.getStatus());
                ticketStepOut.setStepInTime(OffsetDateTime.now());
                ticket.setPositionTime(getMedianOfTickets(ticket.getBranchId(), ticket.getServiceId(),
                        TicketType.STANDARD, ticket.getStepOutPosition()));
                System.out.println("Time: " + ticket.getPositionTime());
                ticket.setStepOut(false);
                ticket.setStepOutPosition(0);

                saveTicket(ticket);
                saveTicketH2(ticket);
                saveTicketStepOut(ticketStepOut);

            } else {
                // cancel ticket
                cancelTicket = true;
            }
        }

        if (cancelTicket) {

            ticket.setTicketType(TicketType.CANCELED.getStatus());
            queueTicketMapperService.deleteTicketByTicketId(stepInTicketPayload.getTicketId());

            saveTicket(ticket);
            saveTicketH2(ticket);

            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.TICKET_CANCELLED.getMessage());
        } else {

            saveTicket(ticket);
            saveTicketH2(ticket);

            List<?> branchCounterList = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                    + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                    new TypeReference<List<BranchCounter>>() {
            });

            for (BranchCounter counter : counterList) {

                List<?> branchCounterServiceTABs = RestUtil.getRequest(
                        dbConnectorMicroserviceURL
                        + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                        + counter.getCounterId(),
                        List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

                List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                        branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
                });

                int sumTicketsWaitingInQueue = 0;

                for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                    int ticketsWaitingInQueue = getNoOfTicketsInQueue(ticket.getBranchId(),
                            bcst.getServiceTAB().getServiceId(), counter.getCounterId());
                    sumTicketsWaitingInQueue += ticketsWaitingInQueue;
                }

                AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                        + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + counter.getCounterId(),
                        AgentCounterConfig.class);

                if (agentCounterConfigEntity != null) {
                    Map<String, String> metaData1 = new HashMap<String, String>();
                    metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                    metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));
                    System.out.println("ticketsWaitingInQueue Notification No --->> " + sumTicketsWaitingInQueue);
                    NotificationSender.sendPushNotificationData(
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                            agentCounterConfigEntity.getDeviceToken(), metaData1);
                }
            }

            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.TICKET_STEPIN.getMessage());
        }
    }

    private OffsetDateTime getMedianOfTickets(Long branchId, Long serviceId, TicketType ticketType, int stepOutPosition)
            throws TaboorQMSServiceException, Exception {
        List<QueueTicketMapper> queueTicketMapperList = queueTicketMapperRepository.findByBranchAndService(branchId,
                serviceId);
        List<Ticket> ticketListSorted = new CopyOnWriteArrayList<Ticket>();
        for (int i = 0; i < queueTicketMapperList.size(); i++) {
            ticketListSorted.add(queueTicketMapperList.get(i).getTicket());
        }
        Collections.sort(ticketListSorted, Comparator.comparing(Ticket::getTransferedStatus)
                .thenComparing(Ticket::getTicketType).thenComparing(Ticket::getPositionTime));

        int count = 0;
        OffsetDateTime response = OffsetDateTime.now().withNano(0);

        if (ticketListSorted != null) {
            if (!ticketListSorted.isEmpty()) {
                if (stepOutPosition == 1) {
                    if (ticketListSorted.get(0).getTicketStatus() < TicketStatus.SERVING.getStatus()
                            & ticketListSorted.get(0).getTicketType() == ticketType.getStatus()) {
                        return ticketListSorted.get(0).getPositionTime().minusSeconds(1);
                    } else {
                        return ticketListSorted.get(0).getPositionTime().plusSeconds(1);
                    }
                }

                for (Ticket t : ticketListSorted) {
                    if (t.getTicketType() == ticketType.getStatus()
                            & t.getTicketStatus() < TicketStatus.SERVING.getStatus()) {
                        count++;
                        System.out.println(count + "T-ID: " + t.getTicketNumber());
                        response = t.getPositionTime().plusSeconds(1);
                        if (count == stepOutPosition - 1) {
                            System.out.println("IN " + count);
                            return t.getPositionTime().plusSeconds(1);
                        }
                    }
                }
                if (count == 0 & ticketType.equals(TicketType.FASTPASS)) {
                    if (ticketListSorted.get(0).getTicketStatus() == TicketStatus.SERVING.getStatus()) {
                        return ticketListSorted.get(1).getPositionTime().minusSeconds(1);
                    }
                    return ticketListSorted.get(0).getPositionTime().minusSeconds(1);
                }
            }
        }
        return response;
    }

    @Override
    public GenStatusResponse cancelCustomerTicket(@Valid StepInOutTicketPayload cancelTicketPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        Ticket ticket = getTicketById(cancelTicketPayload.getTicketId());

        if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
            ticketFastpassService.cancelTicketFastpass(ticket.getTicketId());
        }

        ticket.setTicketType(TicketType.CANCELED.getStatus());
        saveTicket(ticket);
        ticket = saveTicketH2(ticket);
        queueTicketMapperService.deleteTicketByTicketId(ticket.getTicketId());

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.TICKET_CANCEL_1.getNotificationCode());
        metaData.put("ticketNumber", ticket.getTicketNumber());
        metaData.put("ticketId", ticket.getTicketId().toString());
        metaData.put("branchId", ticket.getBranchId().toString());
        metaData.put("serviceId", ticket.getServiceId().toString());
        metaData.put("branchName", branchService.getBranch(ticket.getBranchId()).getBranchName());
        metaData.put("branchAddress", branchService.getBranch(ticket.getBranchId()).getAddress());
        metaData.put("serviceName", serviceTABService.getServiceTAB(ticket.getServiceId()).getServiceName());

        PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
        if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
            userNotificationPayload
                    .setTitle(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
            userNotificationPayload.setDescription(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_FASTPASS_CANCEL_2.getNotificationDescription());
        } else {
            userNotificationPayload
                    .setTitle(PushNotification.TICKET_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
            userNotificationPayload.setDescription(PushNotification.TICKET_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_CANCEL_3.getNotificationDescription());
        }
        userNotificationPayload.setMetaData(metaData);

        String uri = userManagementMicroserviceURL + "/notification/push";
        HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
        httpHeaders.setBearerAuth(session.getSessionToken());
        HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(userNotificationPayload,
                httpHeaders);
        ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                userNotificationEntity, GenStatusResponse.class);

        if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK)
                || pushNotificationResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
            throw new TaboorQMSServiceException(
                    444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
        }

        if (session.getDeviceToken() != null) {
            if (!session.getDeviceToken().isEmpty()) {
                NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                        userNotificationPayload.getDescription(), session.getDeviceToken(), metaData);
            }
        }

        // sending notification to agent app to notify that ticket is removed from Queue
        try {
            List<?> branchCounterList = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                    + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                    new TypeReference<List<BranchCounter>>() {
            });

            for (BranchCounter counter : counterList) {
                List<?> branchCounterServiceTABs = RestUtil.getRequest(
                        dbConnectorMicroserviceURL
                        + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                        + counter.getCounterId(),
                        List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

                List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                        branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
                });

                int sumTicketsWaitingInQueue = 0;

                for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                    int ticketsWaitingInQueue = getNoOfTicketsInQueue(ticket.getBranchId(),
                            bcst.getServiceTAB().getServiceId(), counter.getCounterId());
                    sumTicketsWaitingInQueue += ticketsWaitingInQueue;
                }

                AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                        + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + counter.getCounterId(),
                        AgentCounterConfig.class);

                if (agentCounterConfigEntity != null) {
                    Map<String, String> metaData1 = new HashMap<String, String>();
                    metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                    metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));
                    System.out.println("ticketsWaitingInQueue Notification No --->> " + sumTicketsWaitingInQueue);
                    NotificationSender.sendPushNotificationData(
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                            PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                            agentCounterConfigEntity.getDeviceToken(), metaData1);
                }
            }
        } catch (Exception ex) {
            System.out.println("Catched Exception: " + ex.getLocalizedMessage());
        }

        /////// end notification code
        // send notification to all customer apps in queue to indicate new ticket in
        // queue
        com.taboor.qms.core.model.Queue queueEntity = queueService.getByBranchAndService(ticket.getBranchId(),
                ticket.getServiceId());
        sendNotificationCustomerApps(queueEntity, ticket);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_CANCELLED.getMessage());
    }

    @Override
    public GenStatusResponse addTicketFeedback(@Valid AddTicketFeedbackPayload addTicketFeedbackPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        com.taboor.qms.core.model.Ticket ticket = getTicketEntityById(addTicketFeedbackPayload.getTicketId());

        TicketFeedback ticketFeedback = new TicketFeedback();
        ticketFeedback.setTicket(ticket);
        ticketFeedback.setUser(session.getUser());
        ticketFeedback.setComment(addTicketFeedbackPayload.getComment());
        ticketFeedback.setRating(addTicketFeedbackPayload.getRating());

        if (addTicketFeedbackPayload.getServicePoints().equals("SATISFIED")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.SATISFIED.getStatus());
        } else if (addTicketFeedbackPayload.getServicePoints().equals("AVERAGE")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.AVERAGE.getStatus());
        } else if (addTicketFeedbackPayload.getServicePoints().equals("POOR")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.POOR.getStatus());
        } else {
            ticketFeedback.setServicePoints(ServiceReviewStatus.OTHER.getStatus());
        }

        String uri = dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/save";
        ResponseEntity<TicketFeedback> saveTicketFeedbackResponse = restTemplate.postForEntity(uri, ticketFeedback,
                TicketFeedback.class);
        if (!saveTicketFeedbackResponse.getStatusCode().equals(HttpStatus.OK)
                || saveTicketFeedbackResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKETFEEDBACK_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.TICKETFEEDBACK_NOT_CREATED.getErrorDetails());
        }

        ticket.getBranch().setRating(
                (ticket.getBranch().getRating() * ticket.getBranch().getRatingCount() + ticketFeedback.getRating())
                / (ticket.getBranch().getRatingCount() + 1));
        df.setRoundingMode(RoundingMode.UP);
        ticket.getBranch().setRating(Double.valueOf(df.format(ticket.getBranch().getRating())));
        ticket.getBranch().setRatingCount(ticket.getBranch().getRatingCount() + 1);
        RestUtil.post(dbConnectorMicroserviceURL + "/tab/branches/save", ticket.getBranch(), Branch.class);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_FEEDBACK_ADDED.getMessage());
    }

//////Counter Ticket Feedback
    @Override
    public GenStatusResponse addTicketFeedbackCounter(@Valid AddTicketFeedbackCounterPayload addTicketFeedbackPayload)
            throws TaboorQMSServiceException, Exception {

        com.taboor.qms.core.model.Ticket ticketEntity = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/tickets/getTicket/number?ticketNumber="
                + addTicketFeedbackPayload.getTicketNumber(),
                com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());

        TicketFeedback ticketFeedback = new TicketFeedback();
        ticketFeedback.setTicket(ticketEntity);
        ticketFeedback.setUser(null);
        ticketFeedback.setComment("");

        if (addTicketFeedbackPayload.getServicePoints().equals("SATISFIED")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.SATISFIED.getStatus());
            ticketFeedback.setRating(5.0);
        } else if (addTicketFeedbackPayload.getServicePoints().equals("AVERAGE")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.AVERAGE.getStatus());
            ticketFeedback.setRating(3.0);
        } else if (addTicketFeedbackPayload.getServicePoints().equals("POOR")) {
            ticketFeedback.setServicePoints(ServiceReviewStatus.POOR.getStatus());
            ticketFeedback.setRating(1.0);
        } else {
            ticketFeedback.setServicePoints(ServiceReviewStatus.OTHER.getStatus());
            ticketFeedback.setRating(0.0);
        }

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/save", ticketFeedback,
                TicketFeedback.class, xyzErrorMessage.TICKETFEEDBACK_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.TICKETFEEDBACK_NOT_CREATED.getErrorDetails());

        ticketEntity.getBranch()
                .setRating((ticketEntity.getBranch().getRating() * ticketEntity.getBranch().getRatingCount()
                        + ticketFeedback.getRating()) / (ticketEntity.getBranch().getRatingCount() + 1));
        df.setRoundingMode(RoundingMode.UP);
        ticketEntity.getBranch().setRating(Double.valueOf(df.format(ticketEntity.getBranch().getRating())));
        ticketEntity.getBranch().setRatingCount(ticketEntity.getBranch().getRatingCount() + 1);
        RestUtil.post(dbConnectorMicroserviceURL + "/tab/branches/save", ticketEntity.getBranch(), Branch.class);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_FEEDBACK_ADDED.getMessage());
    }

    @Override
    public GetValuesResposne serveCustomerTicket(@Valid ServeTicketPayload serveTicketPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        Ticket ticket = getTicketById(serveTicketPayload.getTicketId());

        // get counter by agent
        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounteragent/getCounter/agent?agentId="
                + serveTicketPayload.getAgentId(),
                BranchCounter.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        // get ticket details
        ticket.setCounterNumber(branchCounter.getCounterNumber());
        ticket.setTicketStatus(TicketStatus.SERVING.getStatus());
        ticket.setAgentCallTime(OffsetDateTime.now().withNano(0));

        com.taboor.qms.core.model.Ticket ticketEntity = saveTicket(ticket);
        saveTicketH2(ticket);

//		//delete ticket from the queue
//		queueTicketMapperService.deleteTicketByTicketId(serveTicketPayload.getTicketId());
        BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/counter?counterId="
                + branchCounter.getCounterId(),
                BranchCounterAgentMapper.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

        TicketServed ticketServed = new TicketServed();
        ticketServed.setCompletionTime(null);
        ticketServed.setBranchCounter(branchCounter);
        ticketServed.setServedAgent(branchCounterAgentMapper.getAgent());
        ticketServed.setTicket(ticketEntity);
        ticketServed.setAgentCallTime(ticket.getAgentCallTime());
        ticketServed.setTotalTime(null);

        HttpEntity<TicketServed> ticketServedEntity = new HttpEntity<>(ticketServed,
                HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/ticketserveds/save", ticketServedEntity,
                TicketServed.class, xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorDetails());

        HashMap<String, Object> ticketServing = new HashMap<String, Object>();
        ticketServing.put("Counter", branchCounter.getCounterNumber());

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.TICKET_SERVING_TURN.getNotificationCode());
        metaData.put("ticketNumber", ticket.getTicketNumber());
        metaData.put("ticketId", ticket.getTicketId().toString());
        metaData.put("branchId", ticket.getBranchId().toString());
        metaData.put("serviceId", ticket.getServiceId().toString());
        metaData.put("branchName", branchService.getBranch(ticket.getBranchId()).getBranchName());
        metaData.put("branchAddress", branchService.getBranch(ticket.getBranchId()).getAddress());
        metaData.put("serviceName", serviceTABService.getServiceTAB(ticket.getServiceId()).getServiceName());

        PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
        userNotificationPayload
                .setTitle(PushNotification.TICKET_SERVING_TURN.getNotificationTitle() + ticket.getTicketNumber());
        userNotificationPayload.setDescription(
                PushNotification.TICKET_SERVING_TURN.getNotificationDescription() + branchCounter.getCounterNumber());
        userNotificationPayload.setMetaData(metaData);

        String uri = userManagementMicroserviceURL + "/notification/push";
        HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
        httpHeaders.setBearerAuth(session.getSessionToken());
        HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(userNotificationPayload,
                httpHeaders);
        ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                userNotificationEntity, GenStatusResponse.class);

        if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK)
                || pushNotificationResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
            throw new TaboorQMSServiceException(
                    444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
        }

        if (session.getDeviceToken() != null) {
            if (!session.getDeviceToken().isEmpty()) {
                NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                        userNotificationPayload.getDescription(), session.getDeviceToken(), metaData);
            }
        }

        // send notification to counter application to change the serving ticket
        BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                BranchCounterConfig.class);

        if (branchCounterConfig != null) {
            Map<String, String> metaData1 = new HashMap<String, String>();
            metaData1.put("NotificationCode", PushNotification.TICKET_SERVING_COUNTER.getNotificationCode());
            metaData1.put("ticketNumber", ticket.getTicketNumber());
            metaData1.put("counterNumber", branchCounter.getCounterNumber());

            NotificationSender.sendPushNotificationData(PushNotification.TICKET_SERVING_COUNTER.getNotificationTitle(),
                    PushNotification.TICKET_SERVING_COUNTER.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData1);
        }

        // send notification to TV application to change the serving ticket of counter
        BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                BranchTVConfig.class);

        if (branchTVConfig != null) {
            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_COUNTER_TICKET.getNotificationCode());
            metaData2.put("ticketNumber", ticket.getTicketNumber());
            metaData2.put("counterNumber", branchCounter.getCounterNumber());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_COUNTER_TICKET.getNotificationTitle(),
                    PushNotification.CHANGE_COUNTER_TICKET.getNotificationDescription(),
                    branchTVConfig.getDeviceToken(), metaData2);
        }

        GetValuesResposne resposne = new GetValuesResposne();
        resposne.setValues(ticketServing);
        resposne.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
        resposne.setApplicationStatusResponse(xyzResponseMessage.TICKET_SERVING_SUCCESS.getMessage());

        return resposne;
    }

    @Override
    public GetValuesResposne servedCustomerTicket(@Valid ServeTicketPayload serveTicketPayload)
            throws TaboorQMSServiceException, Exception {

        com.taboor.qms.core.model.Ticket ticket = getTicketEntityById(serveTicketPayload.getTicketId());

        HashMap getTicketTimelineResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/tickets/get/timeline?ticketId=" + ticket.getTicketId(),
                HashMap.class, xyzErrorMessage.TICKET_TIMELINE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.TICKET_TIMELINE_NOT_EXISTS.getErrorDetails());

        HashMap<String, Object> ticketTimeline = getTicketTimelineResponse;

        ticketTimeline.forEach((k, v) -> System.out.println(k + " : " + v));

        GetValuesResposne resposne = new GetValuesResposne();
        resposne.setValues(ticketTimeline);
        resposne.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
        resposne.setApplicationStatusResponse(xyzResponseMessage.TICKET_SERVED_SUCCESS.getMessage());

        return resposne;
    }

    @Override
    public GetValuesResposne getCustomerTicketTimer(@Valid StepInOutTicketPayload stepInTicketPayload)
            throws TaboorQMSServiceException, Exception {

        TicketStepOut ticketStepOut = getStepOutTicketById(stepInTicketPayload.getTicketId());

        long min = ticketStepOut.getStepOutTime().until(OffsetDateTime.now(), ChronoUnit.MINUTES);
        long sec = ticketStepOut.getStepOutTime().plusMinutes(min).until(OffsetDateTime.now(), ChronoUnit.SECONDS);

        HashMap<String, Object> timer = new HashMap<String, Object>();
        timer.put("Minutes", min);
        timer.put("Seconds", sec);

        TicketFastpass ticketFastpass = ticketFastpassService
                .checkTicketFastpassByTicketId(stepInTicketPayload.getTicketId());

//		Branch getBranchResponse = RestUtil.getRequest(
//				dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId="
//						+ ticketStepOut.getTicket().getBranch().getBranchId(),
//				Branch.class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
//				xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
        ServiceCenterConfiguration config = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + ticketStepOut.getTicket().getBranch().getServiceCenter().getServiceCenterId(),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

        if (ticketFastpass != null) {
            timer.put("Total Time", config.getFastpassStepoutTime().getMinute());
        } else {
            timer.put("Total Time", config.getNormalStepoutTime().getMinute());
        }

        GetValuesResposne resposne = new GetValuesResposne();
        resposne.setValues(timer);
        resposne.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
        resposne.setApplicationStatusResponse(xyzResponseMessage.TICKET_TIMER_SUCCESS.getMessage());

        return resposne;
    }

    @Override
    public ServiceCenterConfiguration getConfigEntityById(Long branchId) throws TaboorQMSServiceException, Exception {

        Branch getBranchResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + branchId, Branch.class,
                xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(), xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());

        ServiceCenterConfiguration config = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + getBranchResponse.getServiceCenter().getServiceCenterId(),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

        return config;
    }

    private int getNoOfTicketsInQueue(Long branchId, Long serviceId, Long counterId) {

        System.out.println("Params BranchId -->>" + branchId);
        System.out.println("Params ServiceId -->>" + serviceId);

        int ticketsInQueue = queueTicketMapperRepository.countQueueTicketsNotTransferedByCounterId(branchId, serviceId,
                counterId);

        System.out.println("No of Tickets In Queue -->>" + ticketsInQueue);

        return ticketsInQueue;
    }

    @Async
    private void sendNotificationCustomerApps(com.taboor.qms.core.model.Queue queueEntity, Ticket ticket)
            throws RestClientException, TaboorQMSServiceException, JsonGenerationException, JsonMappingException,
            IOException, FirebaseMessagingException {

        // send notification to all customer apps in queue to indicate new ticket in
        // queue
        List<?> queueTicketMapperEntityResponse = null;
        if (queueEntity == null) {
            queueTicketMapperEntityResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/queueticketmapper/get/all/ticket?ticketId=" + ticket.getTicketId(), List.class);
        } else {
            queueTicketMapperEntityResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/queueticketmapper/get/queueId?queueId=" + queueEntity.getQueueId(), List.class);
        }

        List<com.taboor.qms.core.model.QueueTicketMapper> queueTicketMapperEntityList = GenericMapper
                .convertListToObject(queueTicketMapperEntityResponse,
                        new TypeReference<List<com.taboor.qms.core.model.QueueTicketMapper>>() {
                });

        queueTicketMapperEntityList.parallelStream().forEach((qtm) -> {

            TicketUserMapper ticketUserMapperEn = null;
            try {
                ticketUserMapperEn = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                        + "/tab/ticketusermapper/get/ticketId?ticketId=" + qtm.getTicket().getTicketId(),
                        TicketUserMapper.class);
            } catch (RestClientException e) {
                e.printStackTrace();
            } catch (TaboorQMSServiceException e) {
                e.printStackTrace();
            }

            if (ticketUserMapperEn != null) {
                if (ticketUserMapperEn.getUser() != null) {

                    Session userSession = null;
                    try {
                        userSession = RestUtil.getRequest(
                                dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                                + ticketUserMapperEn.getUser().getUserId(),
                                Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                                xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());
                    } catch (RestClientException e) {
                        e.printStackTrace();
                    } catch (TaboorQMSServiceException e) {
                        e.printStackTrace();
                    }

                    Map<String, String> metaData2 = new HashMap<String, String>();
                    metaData2.put("NotificationCode", PushNotification.TICKET_GET_QUEUE.getNotificationCode());
                    metaData2.put("ticketNumber", ticketUserMapperEn.getTicket().getTicketNumber());
                    metaData2.put("ticketId", ticketUserMapperEn.getTicket().getTicketId().toString());
                    metaData2.put("branchId", ticketUserMapperEn.getTicket().getBranch().getBranchId().toString());
                    metaData2.put("serviceId", ticketUserMapperEn.getTicket().getService().getServiceId().toString());

                    PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                    userNotificationPayload.setTitle(
                            PushNotification.TICKET_GET_QUEUE.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload
                            .setDescription(PushNotification.TICKET_GET_QUEUE.getNotificationDescription());
                    userNotificationPayload.setMetaData(metaData2);

                    if (userSession.getDeviceToken() != null) {
                        if (!userSession.getDeviceToken().isEmpty()) {
                            try {
                                NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                        userNotificationPayload.getDescription(), userSession.getDeviceToken(),
                                        userNotificationPayload.getMetaData());
                            } catch (FirebaseMessagingException e) {
                                e.printStackTrace();
                            } catch (TaboorQMSServiceException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                try {
                    System.out.println("GT-ID: " + qtm.getTicket());
                    System.out.println("GT-ID: " + qtm.getTicket().getTicketId());
                    GuestTicket guestTicket = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                            + "/tab/guesttickets/get/ticket?ticketId=" + qtm.getTicket().getTicketId(), GuestTicket.class);

                    if (guestTicket != null) {
                        System.out.println("GT-ID: " + guestTicket.getGuestTicketId());
                        Map<String, String> metaData2 = new HashMap<String, String>();
                        metaData2.put("NotificationCode", PushNotification.TICKET_GET_QUEUE.getNotificationCode());
                        metaData2.put("ticketNumber", guestTicket.getTicket().getTicketNumber());
                        metaData2.put("ticketId", guestTicket.getTicket().getTicketId().toString());
                        metaData2.put("branchId", guestTicket.getTicket().getBranch().getBranchId().toString());
                        metaData2.put("serviceId", guestTicket.getTicket().getService().getServiceId().toString());

                        PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                        userNotificationPayload.setTitle(
                                PushNotification.TICKET_GET_QUEUE.getNotificationTitle() + ticket.getTicketNumber());
                        userNotificationPayload
                                .setDescription(PushNotification.TICKET_GET_QUEUE.getNotificationDescription());
                        userNotificationPayload.setMetaData(metaData2);

                        if (guestTicket.getDeviceToken() != null) {
                            if (!guestTicket.getDeviceToken().isEmpty()) {
                                try {
                                    NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                            userNotificationPayload.getDescription(), guestTicket.getDeviceToken(),
                                            userNotificationPayload.getMetaData());
                                } catch (FirebaseMessagingException e) {
                                    e.printStackTrace();
                                } catch (TaboorQMSServiceException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (RestClientException | TaboorQMSServiceException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        });

//		for (com.taboor.qms.core.model.QueueTicketMapper qtm : queueTicketMapperEntityList) {
//
//					}
    }

//    Function used in CRON Job to expire tickets
    @Override
    public void expireTickets() throws TaboorQMSServiceException, Exception {
//        Fteching tickets with stepOut
        List<?> response = RestUtil.getRequestNoCheck( dbConnectorMicroserviceURL + "/tab/tickets/get/stepOut", List.class);
        if (response != null) {
            List<com.taboor.qms.core.model.Ticket> steppedOutTicketsList = GenericMapper.convertListToObject(response, new TypeReference<List<com.taboor.qms.core.model.Ticket>>() {});
                if (steppedOutTicketsList != null && steppedOutTicketsList.size() > 0) {
                    for (com.taboor.qms.core.model.Ticket steppedOutTicket : steppedOutTicketsList) {
                        //                Getting TicketStepOut Entry

                        TicketStepOut ticketStepOut = getStepOutTicketById(steppedOutTicket.getTicketId());
                        //                Getting TicketUser Mapping
                        ResponseEntity<TicketUserMapper> tickerUserMapperResponse = restTemplate.getForEntity(
                                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + steppedOutTicket.getTicketId(),
                                TicketUserMapper.class);
                        //                If TicketUser Mapping is not found
                        if (!tickerUserMapperResponse.getStatusCode().equals(HttpStatus.OK) || tickerUserMapperResponse.getBody() == null) {
                            throw new TaboorQMSServiceException(xyzErrorMessage.TICKETUSERMAPPER_NOT_EXISTS.getErrorCode() + "::"
                                    + xyzErrorMessage.TICKETUSERMAPPER_NOT_EXISTS.getErrorDetails());
                        } else {
                            TicketUserMapper ticketUserMapper = tickerUserMapperResponse.getBody();
                            //                    Checking if ticket is fastpass
                            TicketFastpass ticketFastpass = ticketFastpassService.checkTicketFastpassByTicketId(steppedOutTicket.getTicketId());
                            //                  Fetching service center configurations to get step out time limit
                            ServiceCenterConfiguration config = RestUtil.getRequest(
                                    dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                                    + branchService.getBranch(steppedOutTicket.getBranch().getBranchId()).getServiceCenter().getServiceCenterId(),
                                    ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                                    xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

                            Boolean cancelTicket = false;
                            Boolean halfTime = false;
                            //                    if ticket is fastpass
                            if (ticketFastpass != null) {
                                if (!(OffsetDateTime.now().minusMinutes(Long.valueOf(config.getFastpassStepoutTime().getMinute()))
                                        .compareTo(ticketStepOut.getStepOutTime()) == 0
                                        || OffsetDateTime.now().minusMinutes(Long.valueOf(config.getFastpassStepoutTime().getMinute()))
                                                .compareTo(ticketStepOut.getStepOutTime()) == -1)) {
                                    // cancel ticket
                                    double balance = ticketFastpass.getCharges();
                                    ticketFastpass.setCharges(0.0);
                                    //                          Getting payment method
                                    String uri = dbConnectorMicroserviceURL + "/tab/paymentmethods/get/name?paymentMethod="
                                            + "Taboor Credit";
                                    ResponseEntity<PaymentMethod> getPaymentMethodResponse = restTemplate.getForEntity(uri,
                                            PaymentMethod.class);
                                    if (!getPaymentMethodResponse.getStatusCode().equals(HttpStatus.OK)
                                            || getPaymentMethodResponse.getBody() == null) {
                                        throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorCode() + "::"
                                                + xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorDetails());
                                    }
                                    //                            gtetting bank card details
                                    uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/userAndPaymentMethodId?userId="
                                            + ticketUserMapper.getUser().getUserId() + "&paymentMethodId="
                                            + getPaymentMethodResponse.getBody().getPaymentMethodId();
                                    ResponseEntity<UserBankCard> getBankCardResponse = restTemplate.getForEntity(uri, UserBankCard.class);
                                    //                            If no details found
                                    if (!getBankCardResponse.getStatusCode().equals(HttpStatus.OK) || getBankCardResponse.getBody() == null) {
                                        throw new TaboorQMSServiceException(xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorCode() + "::"
                                                + xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorDetails());
                                    }
                                    //                            Adding fastpass ticket price and saving
                                    getBankCardResponse.getBody().setBalance(getBankCardResponse.getBody().getBalance() + balance);
                                    uri = dbConnectorMicroserviceURL + "/tab/userbankcards/save";
                                    ResponseEntity<UserBankCard> saveBankCardResponse = restTemplate.postForEntity(uri,
                                            getBankCardResponse.getBody(), UserBankCard.class);
                                    if (!saveBankCardResponse.getStatusCode().equals(HttpStatus.OK)
                                            || saveBankCardResponse.getBody() == null) {
                                        throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode() + "::"
                                                + xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());
                                    }
                                    ticketFastpassService.saveTicketFastpass(ticketFastpass);
                                    cancelTicket = true;
                                } else if(OffsetDateTime.now().minusMinutes((Long.valueOf(config.getNormalStepoutTime().getMinute()))/2)
                                    .compareTo(ticketStepOut.getStepOutTime()) == 0){
                                    halfTime = true;
                                }
                            } else {
                                //                        If ticket is not fastpass
                                if (!(OffsetDateTime.now().minusMinutes(Long.valueOf(config.getNormalStepoutTime().getMinute()))
                                        .compareTo(ticketStepOut.getStepOutTime()) == 0
                                        || OffsetDateTime.now().minusMinutes(Long.valueOf(config.getNormalStepoutTime().getMinute()))
                                                .compareTo(ticketStepOut.getStepOutTime()) == -1)) {
                                    // cancel ticket
                                    cancelTicket = true;
                                } else if(OffsetDateTime.now().minusMinutes((Long.valueOf(config.getNormalStepoutTime().getMinute()))/2)
                                    .compareTo(ticketStepOut.getStepOutTime()) == 0){
                                    halfTime = true;
                                }
                            }
                            if(halfTime){
//                                Sending notification if half time of limit is passed
                                RSocketPayload payload = new RSocketPayload();
                                payload.setNotificationSubject(RSocketNotificationSubject.HALF_LIMIT_STEPOUT);
                                payload.setTypes(Arrays.asList(NotificationType.IN_APP));
                                List<String> infoList = new ArrayList<>();
                                infoList.add(0, "Half Time for your ticket: " + steppedOutTicket.getTicketNumber() + " has passed.");
                                payload.setInformation(infoList);
                                payload.setRecipientId(ticketUserMapper.getUser().getUserId());
                                RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                                            new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
                                
                            } if (cancelTicket) {
                                //                        Setting ticket type to expired
                                steppedOutTicket.setTicketType(TicketType.EXPIRED.getStatus());
                                steppedOutTicket.setStepOut(false);
                                RestUtil.postRequestEntity(
                                    dbConnectorMicroserviceURL + "/tab/tickets/save", steppedOutTicket,
                                    com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_UPDATED.getErrorCode(),
                                    xyzErrorMessage.TICKET_NOT_UPDATED.getErrorDetails());
                            }
                        }
                    }
                }
        }
    }

    //    Function used in CRON Job to delete tickets at closing time
    @Override
    public void deleteTickets() throws TaboorQMSServiceException, Exception {
//        getting branches
        ResponseEntity<Branch[]> response = restTemplate.getForEntity(
                dbConnectorMicroserviceURL + "/tab/branches/get/all", Branch[].class);
        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        } else {
            if (response.getBody() != null) {
                Branch[] branchList = response.getBody();
                if (branchList != null && branchList.length > 0) {
                    for (Branch branch : branchList) {
//                        fetching working hours for branch
                        List<?> branchWorkingHours = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branch.getBranchId(), List.class);
                        List<BranchWorkingHours> branchWorkingHoursList = GenericMapper.convertListToObject(
                                branchWorkingHours, new TypeReference<List<BranchWorkingHours>>() {
                        });
                        branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
                        OffsetDateTime temp = OffsetDateTime.now().withNano(0);
                        for (BranchWorkingHours item : branchWorkingHoursList){
                            if (item.getDay() == temp.getDayOfWeek().getValue()){
                                if (LocalTime.from(temp).isBefore(item.getClosingTime()) && LocalTime.from(temp).isAfter(item.getOpeningTime())){
                                    branch.setBranchStatus(BranchStatus.OPEN.getStatus());
                                }                
                            }
                        }  
                        //                    saving branch to DB
                        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/branches/save", branch,
                            Branch.class, xyzErrorMessage.BRANCH_NOT_UPDATED.getErrorCode(),xyzErrorMessage.BRANCH_NOT_UPDATED.getErrorDetails());
                        if (branch.getBranchStatus() == 2) {
//                                deleting tickets if branch is closed
                            String uri = dbConnectorMicroserviceURL + "/tab/tickets/delete/branch";
                            
                            restTemplate = new RestTemplate();
                            ResponseEntity<?> deleteTicketResponse = restTemplate.postForEntity(uri, branch, int.class);
                            if (!deleteTicketResponse.getStatusCode().equals(HttpStatus.OK)) {
                                throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_NOT_DELETED.getErrorCode() + "::"
                                        + xyzErrorMessage.TICKET_NOT_DELETED.getErrorDetails());
                            }
                            // H2
                            ticketRepository.deleteByBranchId(branch.getBranchId());
                        }
                    }
                }
            }
        }
    }

}
