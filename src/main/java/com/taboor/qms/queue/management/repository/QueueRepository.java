package com.taboor.qms.queue.management.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.queue.management.model.Queue;


@Repository
@Transactional
public interface QueueRepository extends JpaRepository<Queue, Long> {

	@Query("SELECT q FROM Queue q where q.branchId = :branchId and q.serviceId = :serviceId")
	Optional<Queue> findByBranchAndServiceAndType(@Param("branchId") Long branchId, @Param("serviceId") Long serviceId);

}
