package com.taboor.qms.queue.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.queue.management.service.TaboorConfigurationService;

@RestController
@RequestMapping("/taboorConfiguration")
@CrossOrigin(origins = "*")
public class TicketConfigurationController {

//	private static final Logger logger = LoggerFactory.getLogger(TicketConfigurationController.class);

	@Autowired
	TaboorConfigurationService taboorConfigurationService;

//	@ApiOperation(value = "Add Taboor Configuration.", response = CreateSessionResponse.class)
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "Taboor Configuration saved Successfully"),
//			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
//	@RequestMapping(value = "/get/add", method = RequestMethod.POST, produces = "application/json")
//	public @ResponseBody GenStatusResponse addTaboorConfiguration(
//			@RequestBody @Valid AddTaboorConfigurationPayload addTaboorConfigurationPayload)
//			throws Exception, Throwable {
//		logger.info("Calling Add Taboor Configuration - API");
//		return taboorConfigurationService.addTaboorConfiguration(addTaboorConfigurationPayload);
//	}
//
//	@ApiOperation(value = "Get Taboor Configuration by ConfigKey.", response = CreateSessionResponse.class)
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "Taboor Configuration fetched Successfully"),
//			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
//	@RequestMapping(value = "/get/configKey", method = RequestMethod.POST, produces = "application/json")
//	public @ResponseBody GetTaboorConfigurationListResponse getByConfigKey(@RequestBody @Valid String configKey)
//			throws Exception, Throwable {
//		logger.info("Calling Get Taboor Configuration by ConfigKey - API");
//		return taboorConfigurationService.getByConfigKey(configKey);
//	}
//
//	@ApiOperation(value = "Get All Taboor Configurations.", response = CreateSessionResponse.class)
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "Taboor Configurations fetched Successfully"),
//			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
//			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
//			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
//	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody GetTaboorConfigurationListResponse getAll() throws Exception, Throwable {
//		logger.info("Calling Get Taboor Configuration by ConfigKey - API");
//		return taboorConfigurationService.getAll();
//	}
}
