package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GetRequiredDocumentResponse;
import com.taboor.qms.queue.management.service.RequiredDocumentService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/requiredDocument")
@CrossOrigin(origins = "*")
public class RequiredDocumentController {

	private static final Logger logger = LoggerFactory.getLogger(RequiredDocumentController.class);

	@Autowired
	RequiredDocumentService requiredDocumentService;

	@ApiOperation(value = "Get RequiredDocument of Branch's Service.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer requiredDocument booked Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/BranchAndService", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetRequiredDocumentResponse getRequiredDocument(
			@RequestBody @Valid GetByIdListPayload getRequiredDocumentPayload) throws Exception, Throwable {
		logger.info("Calling Get RequiredDocument of Branch's Service. - API");
		return requiredDocumentService.getRequiredDocumentByBranchAndService(getRequiredDocumentPayload.getIds().get(0),
				getRequiredDocumentPayload.getIds().get(1));
	}

}
