package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.queue.management.response.GetQueueTicketListResponse;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;
import com.taboor.qms.queue.management.service.QueueService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/queue")
@CrossOrigin(origins = "*")
public class QueueController {

	private static final Logger logger = LoggerFactory.getLogger(QueueController.class);

	@Autowired
	QueueService queueService;

	@ApiOperation(value = "Get Customer Pending Tickets.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer Pending Tickets fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/pending/get", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetTicketEntityListResponse getCustomerPendingTickets() throws Exception, Throwable {
		logger.info("Calling Get Customer Pending Tickets - API");
		return queueService.getCustomerPendingTickets();
	}

	@ApiOperation(value = "Get Tickets of Specific Queue.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tickets of Specific Queue fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tickets/get", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetQueueTicketListResponse getSpecificQueueTickets(
			@RequestBody @Valid final GetByIdListPayload getByIdListPayload) throws Exception, Throwable {
		logger.info("Calling Get Tickets of Specific Queue - API");
		// 0-Branch Id && 1-ServiceId
		return queueService.getSpecificQueueTickets(getByIdListPayload.getIds().get(0),
				getByIdListPayload.getIds().get(1));
	}

}
