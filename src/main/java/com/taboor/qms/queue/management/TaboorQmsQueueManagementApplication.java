package com.taboor.qms.queue.management;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.TaskScheduler;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.TicketStepOut;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.queue.management.service.TicketService;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan({"com.taboor.qms.queue.management", "com.taboor.qms.core"})
@EntityScan("com.taboor.qms.queue.management.model")
@EnableJpaRepositories(basePackages = {"com.taboor.qms.queue.management.repository"})
@EnableScheduling
public class TaboorQmsQueueManagementApplication implements CommandLineRunner {
    
    private static final Logger logger = LoggerFactory.getLogger(TaboorQmsQueueManagementApplication.class);

    @PersistenceContext
    EntityManager em;

//	@Autowired
//	MatcherRepository matcherRepository;
//
//	@Autowired
//	PrivilegeRepository privilegeRepository;
    public static void main(String[] args) {
        SpringApplication.run(TaboorQmsQueueManagementApplication.class, args);
    }
    
    private RestTemplate restTemplate = new RestTemplate();

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;
    
    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private ApplicationContext applicationContext;


    
    @Override
    public void run(String... args) throws Exception {

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(getClass().getClassLoader()
                        .getResourceAsStream("taboor-firebase-adminsdk-0p6y8-b25b763338.json")))
                .setDatabaseUrl("https://taboor.firebaseio.com").build();

        FirebaseApp.initializeApp(options);
//        MyThread myThread = applicationContext.getBean(MyThread.class);
//        taskScheduler.scheduleAtFixedRate(
//                myThread, 3600000);
    }
    

    @Autowired
    private TicketService ticketservice;
    
    //    CRON Job to expire step out tickets after their limit passes
    @Scheduled(cron = "0 * * ? * *") // Every minute
    public void expireSteppedOutTickets() throws TaboorQMSServiceException, Exception{
        logger.info("Calling Tickets Expiry cron job");
        ticketservice.expireTickets();
    }
    //    CRON Job to delete tickets after branch closes

    @Scheduled(cron = "0 0/5 * ? * *") // Every 5 minute
    public void deleteTicketsAfterClosingTime() throws TaboorQMSServiceException, Exception{
        logger.info("Calling Deletion at closing time cron job");
        ticketservice.deleteTickets();
    }

//	@PostConstruct
//	@Transactional
//	void dumpIntoDb() {
//
//		List<Privilege> savedPrivileges = privilegeRepository.findAll();
//
//		List<Matcher> matchers = saveMatcherList(new MatcherObject("/ticket/customer/book", HttpMethod.POST),
//				new MatcherObject("/guest/ticket/kiosik", HttpMethod.POST),
//				new MatcherObject("/guest/counter/config", HttpMethod.POST),
//				new MatcherObject("/guest/get/counter/ticket", HttpMethod.POST),
//
//				new MatcherObject("/ticket/transfer", HttpMethod.POST),
//				new MatcherObject("/queue/customer/pending/get", HttpMethod.GET),
//				new MatcherObject("/ticket/feedback/add/counter", HttpMethod.POST),
//				new MatcherObject("/branch/getServices/id", HttpMethod.POST),
//				new MatcherObject("/queue/tickets/get", HttpMethod.POST),
//				new MatcherObject("/ticketFastpass/fastpass/apply", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/cancel/ticket", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/stepOut", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/stepIn", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/get/timer", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/serve/ticket", HttpMethod.POST),
//				new MatcherObject("/ticket/customer/ticket/timeline", HttpMethod.POST),
//				new MatcherObject("/guest/ticket/get", HttpMethod.GET),
//				new MatcherObject("/ticket/customer/get/ticket", HttpMethod.POST),
//				new MatcherObject("/branch/get/id", HttpMethod.POST),
//				new MatcherObject("/branch/get/near", HttpMethod.POST),
//				new MatcherObject("/branch/getServices/id", HttpMethod.POST),
//				new MatcherObject("/branch/getCompleteDetails/id", HttpMethod.POST),
//				
//				new MatcherObject("/requiredDocument/get/BranchAndService", HttpMethod.POST),
//				new MatcherObject("/servicecenter/get/all", HttpMethod.GET),
//				new MatcherObject("/service/get/id", HttpMethod.POST),
//				new MatcherObject("/service/get/all", HttpMethod.GET),
//				
//				new MatcherObject("/taboorConfiguration/get/add", HttpMethod.POST),
//				new MatcherObject("/taboorConfiguration/get/configKey", HttpMethod.POST),
//				new MatcherObject("/taboorConfiguration/get/all", HttpMethod.GET),
//				
//				new MatcherObject("/ticketFastpass/get/id", HttpMethod.POST),
//				new MatcherObject("/ticketFastpass/get/user", HttpMethod.GET),
//				new MatcherObject("/service/get/id", HttpMethod.POST),
//				
//				new MatcherObject("/service/get/id", HttpMethod.POST),
//				new MatcherObject("/guest/get/branches", HttpMethod.GET),
//
//				new MatcherObject("/ticketFastpass/fastpass/cancel", HttpMethod.POST),
//
//                // to be removed
//				new MatcherObject("/guest/send/get/feedback", HttpMethod.POST),
//				new MatcherObject("/guest/send/change/ticket", HttpMethod.POST)
//
//				
//				);
//
//		List<Privilege> privileges = new ArrayList<Privilege>();
//		Privilege privilege;
//
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.CUSTOMER_ACTIONS.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.CUSTOMER_ACTIONS);
//		privilege.setMatchers(getMatchers(matchers, "/ticket/customer/book", "/ticket/transfer",
//				"/queue/customer/pending/get", "/branch/getServices/id", "/queue/tickets/get",
//				"/ticketFastpass/fastpass/apply", "/ticket/customer/cancel/ticket", "/ticket/customer/stepOut", "/ticket/customer/stepIn",
//				"/ticket/customer/get/timer", "/ticket/customer/serve/ticket", "/ticket/customer/ticket/timeline", 
//				"/guest/ticket/get", "/ticket/customer/get/ticket", "/branch/getCompleteDetails/id", "/branch/getServices/id",
//				"/branch/get/near", "/branch/get/id", "/requiredDocument/get/BranchAndService", 
//				"/servicecenter/get/all", "/service/get/all", "/service/get/id", "/taboorConfiguration/get/add", "/taboorConfiguration/get/configKey",
//				"/taboorConfiguration/get/all", "/ticketFastpass/get/id", "/ticketFastpass/get/user"));
//		privileges.add(privilege);
//
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.BRANCH_MANAGEMENT.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.BRANCH_MANAGEMENT);
//		privilege.setMatchers(getMatchers(matchers, "/ticket/feedback/add/counter", "/guest/ticket/kiosik", "/guest/get/branches",
//				"/guest/counter/config", "/guest/get/counter/ticket", "/guest/send/get/feedback", "/guest/send/change/ticket"));
//		privileges.add(privilege);
//
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.GUEST_TICKET.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.GUEST_TICKET);
//		privilege.setMatchers(getMatchers(matchers, "/queue/customer/pending/get", 
//				"/branch/getServices/id", "/queue/tickets/get",  "/ticket/customer/cancel/ticket","/ticket/customer/serve/ticket", 
//				"/ticket/customer/ticket/timeline" , "/ticket/customer/get/ticket"
//				));
//		privileges.add(privilege);
//		
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.SERVICECENTER_VIEWONLY.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.SERVICECENTER_VIEWONLY);
//		privilege.setMatchers(getMatchers(matchers, "/queue/tickets/get"));
//		privileges.add(privilege);
//		
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.AGENT_SERVE.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.AGENT_SERVE);
//		privilege.setMatchers(getMatchers(matchers, "/queue/tickets/get", "/ticketFastpass/fastpass/cancel"));
//		privileges.add(privilege);
//
//		privileges = privilegeRepository.saveAll(privileges);
//		System.out.println("Success");
//
//	}
//
//	private List<Matcher> saveMatcherList(MatcherObject... matchers) {
//		List<Matcher> matcherList = new ArrayList<Matcher>();
//		for (MatcherObject entity : matchers) {
//			Matcher matcher = new Matcher();
//			matcher.setName(entity.getMatcherName());
//			matcher.setMethodinfo(entity.getMethodType().name());
//			matcher.setMicroserviceName(MicroserviceName.QUEUE_MANAGEMENT);
//			matcherList.add(matcher);
//		}
//		return matcherRepository.saveAll(matcherList);
//	}
//
//	private Collection<Matcher> getMatchers(List<Matcher> matcherList, String... matcherNames) {
//		List<String> assignMatcherList = Arrays.asList(matcherNames);
//		List<Matcher> matchers = new ArrayList<Matcher>();
//		assignMatcherList.forEach(name -> matchers
//				.add(matcherList.stream().filter(entity -> name.equals(entity.getName())).findAny().orElse(null)));
//		return matchers;
//	}
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server inMemoryH2DatabaseServer() throws SQLException {
        return Server.createTcpServer(
                "-tcp", "-tcpAllowOthers", "-tcpPort", "9050");
    }

}
