package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.payload.AddTicketFeedbackCounterPayload;
import com.taboor.qms.core.payload.AddTicketFeedbackPayload;
import com.taboor.qms.core.payload.BookCustomerTicketPayload;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.ServeTicketPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.queue.management.response.GetTicketEntityListResponse;
import com.taboor.qms.queue.management.response.GetTicketListResponse;
import com.taboor.qms.queue.management.service.TicketService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/ticket")
@CrossOrigin(origins = "*")
public class TicketController {

	private static final Logger logger = LoggerFactory.getLogger(TicketController.class);

	@Autowired
	TicketService ticketService;

	@ApiOperation(value = "Book Customer Ticket.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket booked Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/book", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse bookCustomerTicket(
			@RequestBody @Valid BookCustomerTicketPayload bookCustomerTicketPayload) throws Exception, Throwable {
		logger.info("Calling Book Customer Ticket - API");
		return ticketService.bookCustomerTicket(bookCustomerTicketPayload);
	}

	@ApiOperation(value = "Get Customer Ticket.", response = GetTicketListResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket fetched Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketListResponse getTicketById(@RequestBody @Valid GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Customer Ticket - API");
		return ticketService.getTicketByIdRest(getByIdPayload.getId());
	}

	@ApiOperation(value = "Transfer Ticket to Another Branch.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket transfered Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/transfer", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketEntityListResponse transferTicket(@RequestBody @Valid GetByIdListPayload getIdListPayload)
			throws Exception, Throwable {
		logger.info("Calling Transfer Ticket to Another Branch - API");
		// 0-TicketId & 1-NewBranchId
		return ticketService.transferTicket(getIdListPayload.getIds().get(0), getIdListPayload.getIds().get(1));
	}

	@ApiOperation(value = "Get Ticket Fastpass Estimation.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket Fastpass Estimation fetched Successfully."),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fastpass/estimation", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetValuesResposne getTicketFastpassEstimation(
			@RequestBody @Valid GetByIdPayload getByIdPayload) throws Exception, Throwable {
		logger.info("Calling Get Ticket Fastpass Estimation - API");
		// 0-queueId
		return ticketService.getTicketFastpassEstimation(getByIdPayload.getId());
	}

	@ApiOperation(value = "Step Out Ticket.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket stepped out successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/stepOut", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetValuesResposne stepOutTicket(
			@RequestBody @Valid StepInOutTicketPayload stepOutTicketPayload) throws Exception, Throwable {
		logger.info("Calling Step Out Customer Ticket - API");
		return ticketService.stepOutCustomerTicket(stepOutTicketPayload);
	}

	@ApiOperation(value = "Step In Ticket.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket stepped in successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/stepIn", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse stepInTicket(@RequestBody @Valid StepInOutTicketPayload stepInTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Step In Customer Ticket - API");
		return ticketService.stepInCustomerTicket(stepInTicketPayload);
	}

	@ApiOperation(value = "Check Timer.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get ticket timer"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/get/timer", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse getTicketTimer(
			@RequestBody @Valid StepInOutTicketPayload stepInTicketPayload) throws Exception, Throwable {
		logger.info("Calling Get Timer In Customer Ticket - API");
		return ticketService.getCustomerTicketTimer(stepInTicketPayload);
	}

	@ApiOperation(value = "Cancel Ticket.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket cancelled successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/cancel/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse cancelTicket(@RequestBody @Valid StepInOutTicketPayload cancelTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Cancel Customer Ticket - API");
		return ticketService.cancelCustomerTicket(cancelTicketPayload);
	}

	@ApiOperation(value = "Serving Ticket.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket serving successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/serve/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetValuesResposne serveTicket(@RequestBody @Valid ServeTicketPayload serveTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Serve Customer Ticket - API");
		return ticketService.serveCustomerTicket(serveTicketPayload);
	}

	@ApiOperation(value = "Get Ticket Timeline.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket served successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/ticket/timeline", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetValuesResposne servedTicket(@RequestBody @Valid ServeTicketPayload serveTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Served Customer Ticket - API");
		return ticketService.servedCustomerTicket(serveTicketPayload);
	}

	@ApiOperation(value = "Add Ticket Feedback.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket Feedback added successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/feedback/add", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse addTicketFeedback(
			@RequestBody @Valid AddTicketFeedbackPayload addTicketFeedbackPayload) throws Exception, Throwable {
		logger.info("Calling Add Ticket Feedback - API");
		return ticketService.addTicketFeedback(addTicketFeedbackPayload);
	}
	
	@ApiOperation(value = "Add Ticket Feedback From Counter.", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ticket Feedback added successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/feedback/add/counter", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse addTicketFeedbackCounter(
			@RequestBody @Valid AddTicketFeedbackCounterPayload addTicketFeedbackPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Ticket Feedback From Counter - API");
		return ticketService.addTicketFeedbackCounter(addTicketFeedbackPayload);
	}

	@ApiOperation(value = "Get Ticket By Id.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer ticket got successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/get/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Ticket getTicket(@RequestBody @Valid GetByIdPayload getTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Customer Ticket - API");
		return ticketService.getTicketEntityById(getTicketPayload.getId());
	}
	
	@ApiOperation(value = "Get Config By Id.", response = GetValuesResposne.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "System Config got successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/customer/get/config", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ServiceCenterConfiguration getConfig(@RequestBody @Valid GetByIdPayload getTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Get System Config - API");
		return ticketService.getConfigEntityById(getTicketPayload.getId());
	}

}
