package com.taboor.qms.queue.management.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.GetNearByBranchPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GetBookmarkedBranchListResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchServicesResponse;
import com.taboor.qms.queue.management.response.GetBranchCompleteDetailsResponse;
import com.taboor.qms.queue.management.service.BranchService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/branch")
@CrossOrigin(origins = "*")
public class BranchController {

	private static final Logger logger = LoggerFactory.getLogger(BranchController.class);

	@Autowired
	BranchService branchService;

	@ApiOperation(value = "Get Branch Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetBranchListResponse getBranchDetails(@RequestBody @Valid GetByIdPayload branchId)
			throws Exception, Throwable {
		logger.info("Calling Get Branch Details - API");
		return branchService.getBranchDetails(branchId.getId());
	}
	
	@ApiOperation(value = "Get NearBy Branches Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/get/near", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetBookmarkedBranchListResponse getNearByBranches(
			@RequestBody @Valid final GetNearByBranchPayload payload) throws Exception, Throwable {
		logger.info("Calling Get NearBy Branches - API");
		return branchService.getNearByBranches(payload);
	}

	@ApiOperation(value = "Get Branch Services Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/getServices/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetBranchServicesResponse getBranchServicesDetails(
			@RequestBody @Valid final GetByIdPayload branchId) throws Exception, Throwable {
		logger.info("Calling Get Branch Services Details - API");
		return branchService.getBranchServicesDetails(branchId.getId());
	}

	@ApiOperation(value = "Get Branch Complete Details.", response = CreateSessionResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Branch Details fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/getCompleteDetails/id", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetBranchCompleteDetailsResponse getBranchCompleteDetails(
			@RequestBody @Valid final GetByIdListPayload getByIdListPayload) throws Exception, Throwable {
		logger.info("Calling Get Branch Complete Details - API");
		// 0-Branch Id && 1-ServiceId
		return branchService.getBranchCompleteDetails(getByIdListPayload.getIds().get(0),
				getByIdListPayload.getIds().get(1));
	}

}
